package com.spotifyplaylistvoting.networking.interfaces;

public interface NetworkStateListener {
    void onConnectionOpened();
    void onConnectionClosed();
}
