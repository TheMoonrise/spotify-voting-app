package com.spotifyplaylistvoting.networking.interfaces;

import com.spotifyplaylistvoting.networking.model.Event;

//This interface allows to receive events from the event creation listener
public interface EventManagementListenerCallback {

    void updateEvent(Event event);
    void returnCreateEvent(Event event);

    void createEventNameAlreadyTaken();
}
