package com.spotifyplaylistvoting.networking.interfaces;

import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.model.Track;

//This interface allows to receive events from the menu listener
public interface SongSearchListenerCallback {

    void updateEvent(Event event);
    void returnGetHistory(Track[] history);
    void returnGetPlaylist(Track[] playlist);
    void returnAddTrack();

    void addTrackTrackNotAllowed();
}
