package com.spotifyplaylistvoting.networking.interfaces;

import com.spotifyplaylistvoting.networking.model.Event;

//This interface allows to receive events from the menu listener
public interface MenuListenerCallback {

    void returnGetNearbyEvents(Event[] events);
    void returnJoinEvent(Event event, boolean isHost);

    void joinEventIncorrectPassword();
    void joinEventEventNotAvailable();
}
