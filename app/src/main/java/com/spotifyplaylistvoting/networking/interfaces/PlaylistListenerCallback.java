package com.spotifyplaylistvoting.networking.interfaces;

import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.model.Track;

//This interface allows to receive events from the playlist listener
public interface PlaylistListenerCallback {

    void updateEvent(Event event);
    void returnGetPlaylist(Track[] playlist);
    void updateTrack(Track track);
    void addTrack(Track track);
    void removeTrack(String trackId);
    void returnGetSongAddCooldown(long lastVote, long cooldown);
    void eventEnded();
    void eventNotAvailable();
    void returnGetHistory(Track[] playlist);
}
