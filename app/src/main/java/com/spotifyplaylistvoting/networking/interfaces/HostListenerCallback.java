package com.spotifyplaylistvoting.networking.interfaces;

import com.spotifyplaylistvoting.networking.model.Track;

//This interface allows to receive events from the menu listener
public interface HostListenerCallback {

    void returnGetCurrentlyPlaying(Track track);
    void returnGetNext(Track track);
    void eventEnded();

    void trackNotAvailable();
}
