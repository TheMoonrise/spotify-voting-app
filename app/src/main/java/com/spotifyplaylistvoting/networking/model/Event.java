package com.spotifyplaylistvoting.networking.model;

//Contains data for a singe event
public class Event {

    //The unique id of this event
    public String eventId;
    //The password for this event
    public String eventPassword;
    //The name of this event
    public String eventName;

    //Whether this event requires a password to connect to
    public boolean isPasswordProtected;
    //Whether this event allows songs with explicit content
    public boolean isExplicitAllowed;
    //Whether this event allows duplicate songs
    public boolean isDuplicateAllowed;

    //How long a user must wait before adding in a song
    public long addSongCooldown;
    //The maximum length a song in this event might have
    public long maxSongLength;

    //The genres not allowed on this event
    public String[] bannedGenres;
}
