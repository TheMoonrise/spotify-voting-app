package com.spotifyplaylistvoting.networking.model;

//Contains data for a single track
public class Track {

    //The unique id of this track
    public String trackId;
    //The title of this track
    public String title;
    //The spotify uri of this track
    public String trackUri;
    //Url to the cover image of this track
    public String coverImage;

    //The subtitle involved in this track
    public String[] artists;

    //The duration of this song
    public long duration;
    //The current voting score of this track
    public int votes;
    //The position of this track in the playlist
    //This might be out of date after the playlist was further edited so use only immediately after receiving the update
    public int position;
    //Whether this song contains explicit content
    public boolean isExplicit;

    //Whether is is possible to vote on this track
    public boolean isVotingOpen;
    //Whether this client has already voted on this specific track
    public boolean isVoted;
}
