package com.spotifyplaylistvoting.networking.services;

import android.app.*;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import com.spotify.android.appremote.api.ConnectionParams;
import com.spotify.android.appremote.api.Connector;
import com.spotify.android.appremote.api.SpotifyAppRemote;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.activities.PlaylistActivity;
import com.spotifyplaylistvoting.networking.helper.NetworkServiceBinder;
import com.spotifyplaylistvoting.networking.helper.SpotifyConnectionManager;

public class SpotifyHostService extends Service {

    //The channel id of the playlist notification
    private static final String NOTIFICATION_CHANNEL_STATUS_ID = "SpotifyPlaylistVotingStatusNotification", NOTIFICATION_CHANNEL_EVENT_ID = "SpotifyPlaylistVotingEventNotification";
    private static final int NOTIFICATION_STATUS_ID = 1193, NOTIFICATION_EVENT_ID = 4421;

    //Extras for intent callbacks
    public static final String EVENT_NAME_EXTRA = "EVENT NAME";
    public static final String PLAYLIST_ACTIVE_EXTRA = "PLAYLIST_ACTIVE";

    //Broadcast name to filter messages from this service
    public static final String BROADCAST_EVENT_NAME = "Spotify Host Service Broadcast Event";

    //Static variable showing the current state of the service
    public static boolean IS_PLAYING;

    //The name of the event this service is currently playing for
    private String eventName;

    //Handle of the spotify remote client
    private SpotifyAppRemote spotifyAppRemote;

    //Handle of the network service
    private NetworkService networkService;

    //The connection manager handling interactions with the spotify client
    private SpotifyConnectionManager connectionManager;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //Connect to the network service
        if (networkService == null) {
            Intent networkServiceIntent = new Intent(this, NetworkService.class);
            bindService(networkServiceIntent, networkServiceConnection, Context.BIND_AUTO_CREATE);
        }

        if (intent.hasExtra(EVENT_NAME_EXTRA)) eventName = intent.getStringExtra(EVENT_NAME_EXTRA);

        //Handle different intent extras
        if (spotifyAppRemote == null || !spotifyAppRemote.isConnected()) connectToSpotifyAppRemote();
        else if (connectionManager != null) connectionManager.startPlayback();

        return START_NOT_STICKY;
    }

    //Connection manager for connecting to the network service
    private ServiceConnection networkServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            networkService = ((NetworkServiceBinder) iBinder).networkService();
            if (spotifyAppRemote != null) createSpotifyConnectionManager(spotifyAppRemote, networkService);

            Log.d("SpotifyHostService", "network service connected");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            networkService = null;
            Log.d("SpotifyHostService", "network service disconnected");
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (connectionManager != null) connectionManager.destroy();
        if (networkServiceConnection != null) unbindService(networkServiceConnection);
        disconnectFromSpotifyAppRemote();
    }

    //Makes this service a persistent foreground service
    public void setPlaylistPlaying() {
        Log.d("SpotifyHostService", "Set to playing #1");
        notifyPlaylistState(true);

        Notification notification = playlistNotification();
        startForeground(NOTIFICATION_STATUS_ID, notification);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.cancel(NOTIFICATION_EVENT_ID);

        Log.d("SpotifyHostService", "Set to playing");
    }

    //Makes this service a default service that can be terminated by swiping the notification
    //The provided boolean specifies whether the stop is due to a playback issue
    public void setPlaylistNotPlaying(boolean isPlaybackIssue) {
        stopForeground(true);

        if (IS_PLAYING) {
            String text = isPlaybackIssue ? getResources().getString(R.string.notification_event_text_playback_changed) : getResources().getString(R.string.notification_event_text_empty_playlist);
            String largeText = isPlaybackIssue ? getResources().getString(R.string.notification_event_largetext_playback_changed) : null;

            Notification notification = stopPlaybackNotification(text, largeText);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.notify(NOTIFICATION_EVENT_ID, notification);
        }

        notifyPlaylistState(false);
        Log.d("SpotifyHostService", "Set to not playing");
    }

    //Sends a local broadcast notifying about the current playlist state
    private void notifyPlaylistState(boolean isPlaying) {
        Intent intent = new Intent(BROADCAST_EVENT_NAME);
        intent.putExtra(PLAYLIST_ACTIVE_EXTRA, isPlaying);

        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.sendBroadcast(intent);

        IS_PLAYING = isPlaying;
    }

    //Creates the notification displaying the current playlist state
    private Notification playlistNotification() {
        createStatusNotificationChannel();

        //Create the pending intent to return to the playlist view
        Intent contentIntent = new Intent(this, PlaylistActivity.class);
        contentIntent.putExtra(PlaylistActivity.EVENT_NAME_EXTRA, eventName);
        contentIntent.putExtra(PlaylistActivity.IS_HOST_EXTRA, true);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(contentIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        //Creates the base notification varying some fields depending on play state
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_STATUS_ID)
            .setSmallIcon(R.drawable.ic_notification_player)
            .setContentTitle(getString(R.string.notification_status_title))
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setShowWhen(false)
            .setContentIntent(pendingIntent);

        return builder.build();
    }

    //Creates a notification to inform about the end of the playlist
    private Notification stopPlaybackNotification(String message, String largeText) {
        createEventNotificationChannel();

        //Create the pending intent to return to the playlist view
        Intent contentIntent = new Intent(this, PlaylistActivity.class);
        contentIntent.putExtra(PlaylistActivity.EVENT_NAME_EXTRA, eventName);
        contentIntent.putExtra(PlaylistActivity.IS_HOST_EXTRA, true);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(contentIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        //Creates the base notification varying some fields depending on play state
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_EVENT_ID)
            .setSmallIcon(R.drawable.ic_notification_player)
            .setContentTitle(getString(R.string.notification_event_title))
            .setContentText(message)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setShowWhen(false)
            .setContentIntent(pendingIntent);

        if (largeText != null) builder.setStyle(new NotificationCompat.BigTextStyle().bigText(largeText));

        return builder.build();
    }

    //Creates the notification channel for sdk versions 8 or later for status messages
    private void createStatusNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelName = getString(R.string.notification_channel_status_name);
            String channelDescription = getString(R.string.notification_channel_status_description);

            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_STATUS_ID, channelName, NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(channelDescription);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    //Creates the notification channel for sdk versions 8 or later for event messages
    private void createEventNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelName = getString(R.string.notification_channel_event_name);
            String channelDescription = getString(R.string.notification_channel_event_description);

            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_EVENT_ID, channelName, NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription(channelDescription);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    //Connects to the spotify app to retrieve an instance of app remote
    //This only works when the spotify app is installed - required for hosting
    private void connectToSpotifyAppRemote() {

        String spotifyClientID = getString(R.string.client_id);
        String spotifyRedirectURI = getString(R.string.redirect_uri);

        ConnectionParams params = new ConnectionParams.Builder(spotifyClientID).setRedirectUri(spotifyRedirectURI).showAuthView(true).build();
        SpotifyAppRemote.connect(this, params, new Connector.ConnectionListener() {
            @Override
            public void onConnected(SpotifyAppRemote appRemote) {

                spotifyAppRemote = appRemote;
                if (networkService != null) createSpotifyConnectionManager(spotifyAppRemote, networkService);

                Log.i("SpotifyHostService", "connection to app remote successful");
            }

            @Override
            public void onFailure(Throwable throwable) {

                Log.i("SpotifyHostService", "connection to app remote failed with message: " + throwable.getMessage());
            }
        });
    }

    //Disconnects from the spotify client
    private void disconnectFromSpotifyAppRemote() {

        if (spotifyAppRemote != null) {
            SpotifyAppRemote.disconnect(spotifyAppRemote);
            spotifyAppRemote = null;
        }
    }

    //Creates a new connection manager and begins the playback of the playlist
    private void createSpotifyConnectionManager(SpotifyAppRemote spotifyAppRemote, NetworkService networkService) {
        connectionManager = new SpotifyConnectionManager(this, spotifyAppRemote, networkService);
        connectionManager.startPlayback();
    }
}
