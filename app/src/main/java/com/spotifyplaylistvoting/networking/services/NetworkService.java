package com.spotifyplaylistvoting.networking.services;

import android.app.Service;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.networking.interfaces.NetworkStateListener;
import com.spotifyplaylistvoting.networking.json.DefaultMessageBuilder;
import com.spotifyplaylistvoting.networking.helper.NetworkManager;
import com.spotifyplaylistvoting.networking.helper.NetworkServiceBinder;
import com.spotifyplaylistvoting.networking.json.MessageListener;

import java.util.ArrayList;

//Handles an asynchronous network connection to the server
//Allows for bidirectional communication between server and client
public class NetworkService extends Service implements NetworkManager.Listener{

    //Server detail stored in the config file
    String serverAddress;
    int serverPort;

    //The id of the current device
    private String deviceId;

    //The network manager to handle the socket connection
    private NetworkManager networkManager;

    //The binder providing access to this service
    private NetworkServiceBinder networkServiceBinder;

    //The list of listeners on the client server connection
    private ArrayList<MessageListener> listeners;
    private ArrayList<NetworkStateListener> stateListeners;

    //Whether this service should maintain a server connection
    private boolean isReconnect = true;


    @Override
    public void onCreate() {
        super.onCreate();

        serverAddress = getResources().getString(R.string.serverAddress);
        serverPort = getResources().getInteger(R.integer.serverPort);

        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        networkServiceBinder = new NetworkServiceBinder(this);
        listeners = new ArrayList<>();
        stateListeners = new ArrayList<>();

        createConnection();
        Log.d("NETWORK", "SERVICE CREATED");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (networkManager != null) {
            isReconnect = false;
            networkManager.close();
        }
        Log.d("NETWORK", "SERVICE DESTROYED");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("NETWORK", "SERVICE BOUND");

        return networkServiceBinder;
    }

    @Override
    public void onMessageReceived(String message) {
        for (MessageListener l : listeners)l.onMessageReceived(message);
    }

    @Override
    public void onConnectionClosed() {
        if (isReconnect) createConnection();
    }

    @Override
    public void onConnectionOpened() {
        String message = DefaultMessageBuilder.identifyClient(deviceId);
        networkManager.sendMessage(message);
    }

    //Creates a new network manager to open a new socket connection to the server
    private void createConnection() {
        if (networkManager != null) networkManager.close();
        this.networkManager = new NetworkManager(serverAddress, serverPort, this);
    }

    //Allows listeners to be added to receive messages sent on the network
    public void addListener(MessageListener listener) {
        if (!listeners.contains(listener)) listeners.add(listener);
    }

    public void addListener(NetworkStateListener listener) {
        if (!stateListeners.contains(listener)) stateListeners.add(listener);
    }

    //Removes a listener from the list of registered listeners
    //The must be done to avoid memory leaks
    public void removeListener(MessageListener listener) {
        listeners.remove(listener);
    }

    public void removeListener(NetworkStateListener listener) {
        stateListeners.remove(listener);
    }


    //Sends a message through the open network connection
    //This will fail if no network is available
    public void sendMessage(String message) {
        if (networkManager != null) networkManager.sendMessage(message);
    }
}
