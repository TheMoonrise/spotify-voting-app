package com.spotifyplaylistvoting.networking.helper;

import android.os.Binder;
import com.spotifyplaylistvoting.networking.services.NetworkService;

//Binder that allows activities to access the connected network service
public class NetworkServiceBinder extends Binder {

    //The connected network service using this binder
    private NetworkService networkService;


    public NetworkServiceBinder(NetworkService networkService) {
        this.networkService = networkService;
    }

    //Returns the connected network service
    public NetworkService networkService() {
        return networkService;
    }
}
