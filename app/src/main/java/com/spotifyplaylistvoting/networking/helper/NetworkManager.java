package com.spotifyplaylistvoting.networking.helper;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class NetworkManager extends Thread {

    public static final int CONNECTION_TIMEOUT = 10000;

    private String serverAddress;
    private int serverPort;

    private Socket socket;
    private Listener listener;

    private boolean isConnected;

    private BufferedReader reader;
    private BufferedWriter writer;

    //Listener interface for handling incoming messages
    public interface Listener {

        void onMessageReceived(String message);
        void onConnectionClosed();
        void onConnectionOpened();
    }


    public NetworkManager(String serverAddress, int serverPort, Listener listener) {

        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
        this.listener = listener;

        start();
    }

    @Override
    public void run() {

        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(serverAddress, serverPort), CONNECTION_TIMEOUT);

            isConnected = true;
            Log.i("ServerConnection", "connection to server " + socket.getInetAddress() + " established");

            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            listener.onConnectionOpened();

            while (isConnected) {
                String line = reader.readLine();
                if (line != null) acceptMessage(line);
                else break;
            }

        } catch (SocketTimeoutException e) {
            Log.i("ServerConnection", "failed to connect to server. timeout after " + CONNECTION_TIMEOUT);
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.i("ServerConnection", "connection to server closed");
        listener.onConnectionClosed();
    }

    //Handles incoming messages
    private void acceptMessage(String message) {

        Log.i("ServerConnection", "received message from server: " + message);
        listener.onMessageReceived(message);
    }

    //Sends messages to the server
    //Returns whether the message was delivered successfully
    public void sendMessage(String message) {

        if (!isConnected) return;
        final String finalMessage = message;

        Thread sendMessage = new Thread() {
            @Override
            public void run() {

                try {
                    writer.write(finalMessage);
                    writer.newLine();
                    writer.flush();
                    Log.i("ServerConnection", "sending message to server: " + finalMessage);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("ServerConnection", "sending message to server failed");

                    isConnected = false;
                }
            }
        };

        sendMessage.start();
    }

    //Closes the connection with the server
    public void close() {

        try {
            isConnected = false;
            if (socket != null) socket.close();

        } catch (Exception e) {
            Log.i("ServerConnection", "closing connection to server failed");
            e.printStackTrace();
        }
    }

    //Returns whether the connection so the server is still active
    public boolean isConnected() {
        return isConnected;
    }
}
