package com.spotifyplaylistvoting.networking.helper;

import android.util.Log;
import com.spotify.android.appremote.api.SpotifyAppRemote;
import com.spotify.protocol.client.Subscription;
import com.spotify.protocol.types.PlayerState;
import com.spotify.protocol.types.Repeat;
import com.spotifyplaylistvoting.networking.interfaces.HostListenerCallback;
import com.spotifyplaylistvoting.networking.json.HostListener;
import com.spotifyplaylistvoting.networking.json.PlaylistMessageBuilder;
import com.spotifyplaylistvoting.networking.model.Track;
import com.spotifyplaylistvoting.networking.services.NetworkService;
import com.spotifyplaylistvoting.networking.services.SpotifyHostService;

//Manages the connection to the remote spotify client
public class SpotifyConnectionManager {

    //The spotify host service using this connection manager
    private SpotifyHostService spotifyHostService;

    //The app remote client used for this connection
    private SpotifyAppRemote spotifyAppRemote;

    //The network service used to communicate with the server
    private NetworkService networkService;
    private HostListener hostListener;

    //Tracks whether the playlist is currently being played
    private boolean isPlaylistPlaying, isWaitingForSong;

    //Information about the currently played track
    private String currentTrack;
    private long currentTrackStartTime;


    public SpotifyConnectionManager(SpotifyHostService spotifyHostService, SpotifyAppRemote spotifyAppRemote, NetworkService networkService) {
        this.spotifyHostService = spotifyHostService;
        this.spotifyAppRemote = spotifyAppRemote;
        this.networkService = networkService;

        spotifyAppRemote.getPlayerApi().subscribeToPlayerState().setEventCallback(playStateListener);

        hostListener = new HostListener(new HostListenerCallback() {
            @Override
            public void returnGetCurrentlyPlaying(Track track) {
                playSong(track.trackUri);
            }

            @Override
            public void returnGetNext(Track track) {
                playSong(track.trackUri);
            }

            @Override
            public void eventEnded() {
                stopPlayback(false);
            }

            @Override
            public void trackNotAvailable() {
                stopPlayback(false);
            }
        });

        networkService.addListener(hostListener);
    }

    Subscription.EventCallback<PlayerState> playStateListener = new Subscription.EventCallback<PlayerState>() {
        @Override
        public void onEvent(PlayerState playerState) {

            Log.i("SpotifyHostService", playerState.track.name + " @" + playerState.playbackPosition + " is paused " + playerState.isPaused);

            boolean isFullDuration = System.currentTimeMillis() - currentTrackStartTime > Math.max(playerState.track.duration - 2000, 0);
            boolean isSongFinished = playerState.playbackPosition == 0 && playerState.isPaused;
            boolean isTrackSwitched = !currentTrack.equals(playerState.track.uri);

            Log.i("SpotifyHostService", "isFullDuration " + isFullDuration + " isSongFinished " + isSongFinished + " isTrackSwitched " + isTrackSwitched + " isPlaylistPlaying " + isPlaylistPlaying);

            if (!isPlaylistPlaying && !isTrackSwitched && !playerState.isPaused) {
                isPlaylistPlaying = true;
                spotifyHostService.setPlaylistPlaying();
            }

            if (isPlaylistPlaying && !isWaitingForSong && isFullDuration && isSongFinished && !isTrackSwitched) playNextSong();
            else if (isPlaylistPlaying && !isWaitingForSong && (isTrackSwitched || playerState.isPaused)) stopPlayback(true);
        }
    };

    //Plays the song with the provided id
    private void playSong(String songId) {
        currentTrack = songId;
        currentTrackStartTime = System.currentTimeMillis();

        spotifyAppRemote.getPlayerApi().setRepeat(Repeat.OFF);
        spotifyAppRemote.getPlayerApi().play(songId);
        isWaitingForSong = false;
    }

    //Plays the next song of the playlist
    private void playNextSong() {
        String message = PlaylistMessageBuilder.getNext();
        networkService.sendMessage(message);
        isWaitingForSong = true;
    }

    //Starts the playback of the playlist
    public void startPlayback() {
        String message = PlaylistMessageBuilder.getCurrentlyPlaying();
        networkService.sendMessage(message);
        isWaitingForSong = true;
        Log.d("SpotifyHostService", "started playlist playback");
    }

    //Stops the playback of the playlist
    public void stopPlayback(boolean isPlaybackIssue) {
        isPlaylistPlaying = false;
        currentTrack = null;

        spotifyHostService.setPlaylistNotPlaying(isPlaybackIssue);
        Log.d("SpotifyHostService", "stopped playlist playback");
    }

    //Clears all dependencies of this class
    public void destroy() {
        spotifyAppRemote.getPlayerApi().subscribeToPlayerState().cancel();
        spotifyAppRemote = null;

        networkService.removeListener(hostListener);
        networkService = null;
    }
}
