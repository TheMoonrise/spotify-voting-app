package com.spotifyplaylistvoting.networking.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.spotifyplaylistvoting.networking.interfaces.EventManagementListenerCallback;
import com.spotifyplaylistvoting.networking.interfaces.PlaylistListenerCallback;
import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.model.Track;

import java.util.ArrayList;

public class PlaylistListener extends MessageListener{

    //The listener receiving the results of incoming messages
    private PlaylistListenerCallback callback;


    public PlaylistListener(PlaylistListenerCallback callback){
        this.callback = callback;
    }

    @Override
    protected void handleMessage(String type, JsonObject data) {
        switch (type) {
            case "updateEvent":
                updateEvent(data);
                break;
            case "returnGetPlaylist":
                returnGetPlaylist(data);
                break;
            case "updateTrack":
                updateTrack(data);
                break;
            case "addTrack":
                addTrack(data);
                break;
            case "removeTrack":
                removeTrack(data);
                break;
            case "eventEnded":
                callback.eventEnded();
                break;
            case "returnGetHistory":
                returnGetHistory(data);
                break;
            case "returnGetSongAddCooldown":
                returnGetSongAddCooldown(data);
                break;
        }
    }

    @Override
    protected void handleError(String type, String error) {
        switch (type) {
            case "updateEvent":
                if (error.equals("eventNotAvailable")) callback.eventNotAvailable();
                break;
        }
    }

    private void updateEvent(JsonObject data) {
        JsonObject eventJson = data.getAsJsonObject("event");
        Event event = gson.fromJson(eventJson, Event.class);
        callback.updateEvent(event);
    }

    private void returnGetPlaylist(JsonObject data) {
        JsonArray playlistJson = data.getAsJsonArray("playlist");
        ArrayList<Track> tracks = new ArrayList<>();

        for(JsonElement t : playlistJson) tracks.add(gson.fromJson(t, Track.class));
        callback.returnGetPlaylist(tracks.toArray(new Track[0]));
    }

    private void updateTrack(JsonObject data) {
        JsonObject trackJson = data.getAsJsonObject("track");
        Track track = gson.fromJson(trackJson, Track.class);
        callback.updateTrack(track);
    }

    private void addTrack(JsonObject data) {
        JsonObject trackJson = data.getAsJsonObject("track");
        Track track = gson.fromJson(trackJson, Track.class);
        callback.addTrack(track);
    }

    private void removeTrack(JsonObject data) {
        String trackId = data.get("trackId").getAsString();
        callback.removeTrack(trackId);
    }

    private void returnGetHistory(JsonObject data) {
        JsonArray playlistJson = data.getAsJsonArray("history");
        ArrayList<Track> tracks = new ArrayList<>();

        for(JsonElement t : playlistJson) tracks.add(gson.fromJson(t, Track.class));
        callback.returnGetHistory(tracks.toArray(new Track[0]));
    }

    private void returnGetSongAddCooldown(JsonObject data) {
        long lastVote = data.get("lastVote").getAsLong();
        long cooldown = data.get("cooldown").getAsLong();
        callback.returnGetSongAddCooldown(lastVote, cooldown);
    }
}
