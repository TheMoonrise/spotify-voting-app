package com.spotifyplaylistvoting.networking.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.spotifyplaylistvoting.networking.interfaces.PlaylistListenerCallback;
import com.spotifyplaylistvoting.networking.interfaces.SongSearchListenerCallback;
import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.model.Track;

import java.util.ArrayList;

public class SongSearchListener extends MessageListener{

    //The listener receiving the results of incoming messages
    private SongSearchListenerCallback callback;


    public SongSearchListener(SongSearchListenerCallback callback){
        this.callback = callback;
    }

    @Override
    protected void handleMessage(String type, JsonObject data) {
        switch (type) {
            case "updateEvent":
                updateEvent(data);
                break;
            case "returnAddTrack":
                callback.returnAddTrack();
                break;
            case "returnGetPlaylist":
                returnGetPlaylist(data);
                break;
            case "returnGetHistory":
                returnGetHistory(data);
                break;
        }
    }

    @Override
    protected void handleError(String type, String error) {
        switch (type) {
            case "returnAddTrack":
                if (error.equals("trackNotAllowed")) callback.addTrackTrackNotAllowed();
                break;
        }
    }

    private void updateEvent(JsonObject data) {
        JsonObject eventJson = data.getAsJsonObject("event");
        Event event = gson.fromJson(eventJson, Event.class);
        callback.updateEvent(event);
    }

    private void returnGetPlaylist(JsonObject data) {
        JsonArray playlistJson = data.getAsJsonArray("playlist");
        ArrayList<Track> tracks = new ArrayList<>();

        for(JsonElement t : playlistJson) tracks.add(gson.fromJson(t, Track.class));
        callback.returnGetPlaylist(tracks.toArray(new Track[0]));
    }

    private void returnGetHistory(JsonObject data) {
        JsonArray playlistJson = data.getAsJsonArray("history");
        ArrayList<Track> tracks = new ArrayList<>();

        for(JsonElement t : playlistJson) tracks.add(gson.fromJson(t, Track.class));
        callback.returnGetHistory(tracks.toArray(new Track[0]));
    }
}
