package com.spotifyplaylistvoting.networking.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public abstract class DefaultMessageBuilder {

    //Finalizes messages to be sent to the server
    public static String buildMessage(String type, JsonObject data) {
        JsonObject json = new JsonObject();

        json.addProperty("type", type);
        json.add("data", data);

        return json.toString();
    }

    //Message to identify the client on the server
    //Currently the mac address of the host device is used for this purpose
    public static String identifyClient(String id) {
        JsonObject data = new JsonObject();
        data.addProperty("userId", id);

        return buildMessage("identifyClient", data);
    }

    //Builds a json representation of a track
    public static JsonObject track(String title, String trackUri, String coverImage, String[] artists, long duration, boolean isExplicit) {
        JsonObject track = new JsonObject();
        track.addProperty("title", title);
        track.addProperty("trackUri", trackUri);
        track.addProperty("coverImage", coverImage);
        track.addProperty("duration", duration);
        track.addProperty("isExplicit", isExplicit);

        JsonArray artistsJson = new JsonArray();
        for (String s : artists) artistsJson.add(s);
        track.add("artists", artistsJson);

        return track;
    }
}
