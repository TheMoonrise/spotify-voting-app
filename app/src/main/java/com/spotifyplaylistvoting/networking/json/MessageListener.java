package com.spotifyplaylistvoting.networking.json;

import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

//Superclass for listeners on the client server communication
public abstract class MessageListener {

    //The parser for interpreting string messages
    private JsonParser parser = new JsonParser();

    //The gson for deserialization
    protected Gson gson = new Gson();

    public final void onMessageReceived(String message){
        try {
            JsonObject json = parser.parse(message).getAsJsonObject();
            JsonObject data = json.get("data").getAsJsonObject();

            String type = json.get("type").getAsString();
            boolean isError = json.get("error").getAsBoolean();

            if (isError) handleError(type, data.get("errorMessage").getAsString());
            else  handleMessage(type, data);

        } catch (Exception e) {
            Log.e("ServerCommunication", "Failed to handle message: " + message);
            e.printStackTrace();
        }
    }

    //Handles correct responses from the server
    protected abstract void handleMessage(String type, JsonObject data);

    //Handles error messages sent from the server
    protected abstract void handleError(String type, String error);
}
