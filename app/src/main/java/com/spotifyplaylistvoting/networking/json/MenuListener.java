package com.spotifyplaylistvoting.networking.json;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.spotifyplaylistvoting.networking.interfaces.MenuListenerCallback;
import com.spotifyplaylistvoting.networking.model.Event;

import java.util.ArrayList;

//Listener for networking events concerning the menu activity
public class MenuListener extends MessageListener {

    //The listener receiving the results of incoming messages
    private MenuListenerCallback callback;


    public MenuListener(MenuListenerCallback callback){
        this.callback = callback;
    }

    @Override
    protected void handleMessage(String type, JsonObject data) {
        switch (type) {
            case "returnGetNearbyEvents":
                returnGetNearbyEvents(data);
                break;
            case "returnJoinEvent":
                returnJoinEvent(data);
                break;
        }
    }

    @Override
    protected void handleError(String type, String error) {
        switch (type) {
            case "returnCreateEvent":
                if (error.equals("incorrectPassword")) callback.joinEventIncorrectPassword();
                if (error.equals("eventNotAvailable")) callback.joinEventEventNotAvailable();
                break;
        }
    }

    private void returnGetNearbyEvents(JsonObject data) {
        JsonArray eventsJson = data.getAsJsonArray("events");
        ArrayList<Event> events = new ArrayList<>();

        for(JsonElement e : eventsJson) events.add(gson.fromJson(e, Event.class));
        callback.returnGetNearbyEvents(events.toArray(new Event[0]));
    }

    private void returnJoinEvent(JsonObject data) {
        JsonObject eventJson = data.getAsJsonObject("event");
        Event event = gson.fromJson(eventJson, Event.class);

        boolean isHost = data.get("isHost").getAsBoolean();
        callback.returnJoinEvent(event, isHost);
    }
}
