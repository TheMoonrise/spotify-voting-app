package com.spotifyplaylistvoting.networking.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.spotifyplaylistvoting.networking.model.Track;

public abstract class PlaylistMessageBuilder {

    public static String getPlaylist() {
        JsonObject data = new JsonObject();
        return DefaultMessageBuilder.buildMessage("getPlaylist", data);
    }

    public static String voteTrack(String trackId, boolean isUpvote) {
        JsonObject data = new JsonObject();
        data.addProperty("trackId", trackId);
        data.addProperty("isUpvote", isUpvote);
        return DefaultMessageBuilder.buildMessage("voteTrack", data);
    }

    public static String addTrack(String title, String trackUri, String coverImage, String[] artists, long duration, boolean isExplicit) {
        JsonObject data = DefaultMessageBuilder.track(title, trackUri, coverImage, artists, duration, isExplicit);
        return DefaultMessageBuilder.buildMessage("addTrack", data);
    }

    public static String addTracks(Track[] tracks) {
        JsonObject data = new JsonObject();
        JsonArray tracksJson = new JsonArray();

        for (Track t : tracks) {
            tracksJson.add(DefaultMessageBuilder.track(t.title, t.trackUri, t.coverImage, t.artists, t.duration, t.isExplicit));
        }

        data.add("tracks", tracksJson);
        return DefaultMessageBuilder.buildMessage("addTracks", data);
    }

    public static String getCurrentlyPlaying() {
        JsonObject data = new JsonObject();
        return DefaultMessageBuilder.buildMessage("getCurrentlyPlaying", data);
    }

    public static String getNext() {
        JsonObject data = new JsonObject();
        return DefaultMessageBuilder.buildMessage("getNext", data);
    }

    public static String getHistory() {
        JsonObject data = new JsonObject();
        return DefaultMessageBuilder.buildMessage("getHistory", data);
    }

    public static String endEvent() {
        JsonObject data = new JsonObject();
        return DefaultMessageBuilder.buildMessage("endEvent", data);
    }

    public static String getSongAddCooldown() {
        JsonObject data = new JsonObject();
        return DefaultMessageBuilder.buildMessage("getSongAddCooldown", data);
    }
}
