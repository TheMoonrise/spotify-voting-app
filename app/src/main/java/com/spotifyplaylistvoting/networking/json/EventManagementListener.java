package com.spotifyplaylistvoting.networking.json;

import com.google.gson.JsonObject;
import com.spotifyplaylistvoting.networking.interfaces.EventManagementListenerCallback;
import com.spotifyplaylistvoting.networking.model.Event;

public class EventManagementListener extends MessageListener{

    //The listener receiving the results of incoming messages
    private EventManagementListenerCallback callback;


    public EventManagementListener(EventManagementListenerCallback callback){
        this.callback = callback;
    }

    @Override
    protected void handleMessage(String type, JsonObject data) {
        switch (type) {
            case "returnCreateEvent":
                returnCreateEvent(data);
                break;
            case "updateEvent":
                updateEvent(data);
                break;
        }
    }

    @Override
    protected void handleError(String type, String error) {
        switch (type) {
            case "returnCreateEvent":
                if (error.equals("nameAlreadyTaken")) callback.createEventNameAlreadyTaken();
                break;
        }
    }

    private void updateEvent(JsonObject data) {
        JsonObject eventJson = data.getAsJsonObject("event");
        Event event = gson.fromJson(eventJson, Event.class);
        callback.updateEvent(event);
    }

    private void returnCreateEvent(JsonObject data) {
        JsonObject eventJson = data.getAsJsonObject("event");
        Event event = gson.fromJson(eventJson, Event.class);

        callback.returnCreateEvent(event);
    }
}
