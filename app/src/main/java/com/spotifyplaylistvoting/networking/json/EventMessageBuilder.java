package com.spotifyplaylistvoting.networking.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public abstract class EventMessageBuilder {

    public static String getNearbyEvents(double latitude, double longitude) {
        JsonObject data = new JsonObject();
        data.addProperty("latitude", latitude);
        data.addProperty("longitude", longitude);
        return DefaultMessageBuilder.buildMessage("getNearbyEvents", data);
    }

    public static String joinEvent(String eventId, String password) {
        JsonObject data = new JsonObject();
        data.addProperty("eventId", eventId);
        data.addProperty("password", password);
        return DefaultMessageBuilder.buildMessage("joinEvent", data);
    }

    public static String createEvent(String name, String password, double latitude, double longitude, double cooldown, double maxSongLength, boolean isExplicitAllowed, boolean isDuplicateAllowed, String[] bannedGenres) {
        JsonObject data = new JsonObject();
        data.addProperty("eventName", name);
        data.addProperty("latitude", latitude);
        data.addProperty("longitude", longitude);
        data.addProperty("addSongCooldown", cooldown);
        data.addProperty("maxSongLength", maxSongLength);
        data.addProperty("isExplicitAllowed", isExplicitAllowed);
        data.addProperty("isDuplicateAllowed", isDuplicateAllowed);

        if (password == null) data.add("password", null);
        else data.addProperty("password", password);

        JsonArray genres = new JsonArray();
        for (String s : bannedGenres) genres.add(s);
        data.add("bannedGenres", genres);

        return DefaultMessageBuilder.buildMessage("createEvent", data);
    }

    public static String editEvent(double cooldown, double maxSongLength, boolean isExplicitAllowed, boolean isDuplicateAllowed, String[] bannedGenres) {
        JsonObject data = new JsonObject();
        data.addProperty("addSongCooldown", cooldown);
        data.addProperty("maxSongLength", maxSongLength);
        data.addProperty("isExplicitAllowed", isExplicitAllowed);
        data.addProperty("isDuplicateAllowed", isDuplicateAllowed);

        JsonArray genres = new JsonArray();
        for (String s : bannedGenres) genres.add(s);
        data.add("bannedGenres", genres);

        return DefaultMessageBuilder.buildMessage("editEvent", data);
    }

    public static String getCurrentEvent() {
        JsonObject data = new JsonObject();
        return DefaultMessageBuilder.buildMessage("getCurrentEvent", data);
    }
}
