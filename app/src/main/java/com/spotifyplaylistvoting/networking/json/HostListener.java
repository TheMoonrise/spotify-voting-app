package com.spotifyplaylistvoting.networking.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.spotifyplaylistvoting.networking.interfaces.HostListenerCallback;
import com.spotifyplaylistvoting.networking.model.Track;

public class HostListener extends MessageListener{

    //The listener receiving the results of incoming messages
    private HostListenerCallback callback;


    public HostListener(HostListenerCallback callback){
        this.callback = callback;
    }

    @Override
    protected void handleMessage(String type, JsonObject data) {
        switch (type) {
            case "returnGetCurrentlyPlaying":
                returnGetCurrentlyPlaying(data);
                break;
            case "returnGetNext":
                returnGetNext(data);
                break;
            case "eventEnded":
                callback.eventEnded();
                break;
        }
    }

    @Override
    protected void handleError(String type, String error) {
        switch (type) {
            case "returnGetCurrentlyPlaying":
                if (error.equals("trackNotAvailable")) callback.trackNotAvailable();
                break;
            case "returnGetNext":
                if (error.equals("trackNotAvailable")) callback.trackNotAvailable();
                break;
        }
    }

    private void returnGetCurrentlyPlaying(JsonObject data) {
        JsonObject trackJson = data.getAsJsonObject("track");
        Track track = gson.fromJson(trackJson, Track.class);
        callback.returnGetCurrentlyPlaying(track);
    }

    private void returnGetNext(JsonObject data) {
        JsonObject trackJson = data.getAsJsonObject("track");
        Track track = gson.fromJson(trackJson, Track.class);
        callback.returnGetNext(track);
    }
}
