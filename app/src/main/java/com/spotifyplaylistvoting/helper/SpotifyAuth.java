package com.spotifyplaylistvoting.helper;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import butterknife.BindString;
import butterknife.ButterKnife;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotifyplaylistvoting.R;

public class SpotifyAuth {

    //Sting resource references
    @BindString(R.string.client_id) String spotifyClientID;
    @BindString(R.string.redirect_uri) String spotifyRedirectURI;

    //Intent request codes
    public static final int AUTHENTICATION_REQUEST_CODE = 2243;

    //The attached activity
    private Activity activity;

    //The spotify auth key
    private String spotifyAuthKey;


    public SpotifyAuth(Activity activity) {
        this.activity =activity;
        ButterKnife.bind(this, activity);
    }

    //Retrieves the token from the response
    public boolean isRequestSuccessful(int requestCode, int resultCode, Intent intent) {
        if (requestCode == AUTHENTICATION_REQUEST_CODE) {
            AuthenticationResponse authenticationResponse = AuthenticationClient.getResponse(resultCode, intent);

            if (authenticationResponse.getType() == AuthenticationResponse.Type.TOKEN) {
                spotifyAuthKey = authenticationResponse.getAccessToken();
                Log.i("SpotifyAuth", "spotify authentication success. Expires in: " + authenticationResponse.getExpiresIn() + " Token: " + spotifyAuthKey);
                return true;
            }

            Log.i("SpotifyAuth", "spotify authentication failed. Error: " + authenticationResponse.getError());
        }
        return false;
    }

    //Request user authorization to retrieve and auth key for the spotify web api
    public void authorizeSpotifyUser() {

        AuthenticationRequest.Builder authenticationBuilder = new AuthenticationRequest.Builder(spotifyClientID, AuthenticationResponse.Type.TOKEN, spotifyRedirectURI);

        //Setting all required scopes - refer to https://developer.spotify.com/documentation/general/guides/scopes/ for more details
        authenticationBuilder.setScopes(new String[]{"app-remote-control", "streaming"});

        AuthenticationRequest authenticationRequest = authenticationBuilder.build();
        AuthenticationClient.openLoginActivity(activity, AUTHENTICATION_REQUEST_CODE, authenticationRequest);
    }

    //Returns the spotify auth key
    public String authKey() {
        return spotifyAuthKey;
    }
}
