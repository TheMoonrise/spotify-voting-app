package com.spotifyplaylistvoting.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import androidx.collection.LruCache;
import com.android.volley.*;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

//Contains a volley queue and allows to make requests to the spotify servers
public class VolleySpotifyQueue {

    //The request code for web requests
    private static final String HTTP_REQUEST_TAG = "Spotify Server Request";

    //The context this view is used in
    private Context context;

    //The spotify authentication key
    private String spotifyAuthKey;

    //The request queue and image loader
    private RequestQueue queue;
    private ImageLoader imageLoader;

    //Interface for handling responses from the server
    public interface ResponseListener {
        void onResponse(String response);
    }

    public VolleySpotifyQueue(Context context, String spotifyAuthKey) {
        this.context = context;
        this.spotifyAuthKey = spotifyAuthKey;

        queue = Volley.newRequestQueue(context);

        imageLoader = new ImageLoader(queue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(10);
            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });
    }

    //Sends a request to receive info to the spotify web api
    public void sendRequest(String url, final ResponseListener responseListener) {
        try {
            StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    responseListener.onResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Log.i("SpotifySearch", "an error occurred when accessing the spotify web api: " + new String(error.networkResponse.data, "utf-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap();
                    headers.put("Authorization", "Bearer " + spotifyAuthKey);
                    return headers;
                }
            };

            request.setTag(HTTP_REQUEST_TAG);
            request.getHeaders();
            queue.add(request);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Cancels all requests
    public void cancelRequests() {
        if (queue != null) queue.cancelAll(HTTP_REQUEST_TAG);
    }

    //Assembles an request url
    public String url(String url, String query, String params) {
        return url + query + params;
    }

    public String url(String url, String query) {
        return url + query;
    }

    public RequestQueue queue() {
        return queue;
    }

    public ImageLoader imageLoader() {
        return imageLoader;
    }
}
