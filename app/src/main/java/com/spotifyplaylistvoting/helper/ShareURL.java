package com.spotifyplaylistvoting.helper;

import android.net.Uri;
import androidx.annotation.NonNull;

public class ShareURL {

    //The url at which the query is directed
    private final String URL = "http://votify.moonrise-games.com/playstore";

    //The query parameters for the url
    private final String EVENT_ID = "e", PASSWORD = "p";

    //The password of the contained event
    private String password;

    //The id of the contained event
    private String id;


    public ShareURL(String id, String password) {
        this.id = id;
        if (password != null) this.password = Encryption.encryptMsg(password, Encryption.secret());
    }

    public ShareURL(Uri uri) {
        String password = uri.getQueryParameter(PASSWORD);
        if (password != null) this.password = Encryption.decryptMsg(password, Encryption.secret());

        this.id = uri.getQueryParameter(EVENT_ID);
    }

    @NonNull
    @Override
    public String toString() {
        String url = URL + "?" + EVENT_ID + "=" + id;
        if (password != null) url += "&" + PASSWORD + "=" + password;
        return url;
    }

    public boolean isValid() {
        return id != null;
    }

    public String id() {
        return id;
    }

    public String password() {
        return password == null ? "" : password;
    }
}
