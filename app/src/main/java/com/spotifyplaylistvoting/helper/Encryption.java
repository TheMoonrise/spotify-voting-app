package com.spotifyplaylistvoting.helper;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Encryption {

    //The encryption secret
    //Must be 16, 24 or 32 bytes
    private final static String token = "*E4T{+-bwEYr5u}{2PK$D&5-";

    //Generates the application wide secret key
    public static SecretKey secret() {
        return new SecretKeySpec(token.getBytes(), "AES");
    }

    //Encrypts a string message
    public static String encryptMsg(String message, SecretKey secret) {

        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secret);
            byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));
            return toHex(cipherText);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //Decrypts a string message
    public static String decryptMsg(String cipherText, SecretKey secret) {

        try {
            byte[] bytes = toByte(cipherText);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secret);
            String decryptString = new String(cipher.doFinal(bytes), "UTF-8");
            return decryptString;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] toByte(String hexString) {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++)
            result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2),
                                        16).byteValue();
        return result;
    }

    public static String toHex(byte[] buf) {
        if (buf == null)
            return "";
        StringBuffer result = new StringBuffer(2 * buf.length);
        for (int i = 0; i < buf.length; i++) {
            appendHex(result, buf[i]);
        }
        return result.toString();
    }

    private final static String HEX = "0123456789ABCDEF";

    private static void appendHex(StringBuffer sb, byte b) {
        sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
    }
}
