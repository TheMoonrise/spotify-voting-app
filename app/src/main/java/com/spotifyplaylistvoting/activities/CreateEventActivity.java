package com.spotifyplaylistvoting.activities;

import android.content.*;
import android.os.IBinder;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import butterknife.OnClick;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.networking.helper.NetworkServiceBinder;
import com.spotifyplaylistvoting.networking.interfaces.EventManagementListenerCallback;
import com.spotifyplaylistvoting.networking.json.EventManagementListener;
import com.spotifyplaylistvoting.networking.json.EventMessageBuilder;
import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.services.NetworkService;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateEventActivity extends AppCompatActivity {

    @BindView(R.id.createEventEditTextName) EditText editTextPartyName;
    @BindView(R.id.createEventEditTextPassword) EditText editTextPassword;
    @BindView(R.id.eventSettingsEditTextSongLength) EditText editTextSongLength;
    @BindView(R.id.eventSettingsEditTextCooldown) EditText editTextCooldown;
    @BindView(R.id.eventSettingsEditTextGenres) EditText editTextGenres;

    @BindView(R.id.eventSettingsExplicitSwitch) Switch switchExplicit;
    @BindView(R.id.eventSettingsDuplicateSwitch) Switch switchDuplicates;

    @BindString(R.string.create_event_empty_partyname_toast) String emptyPartyNameToast;

    //Extras for providing additional intent information
    public static final String LATITUDE_EXTRA = "Latitude", LONGITUDE_EXTRA = "Longitude";

    //Network service for server communication
    private NetworkService networkService;
    private ServiceConnection serviceConnection;
    private EventManagementListener eventManagementListener;

    //The event settings fragment
    private EventSettingsFragment eventSettingsFragment;

    //Location services to access user location
    private FusedLocationProviderClient client;
    private double latitude;
    private double longitude;

    //Listener callback for network events
    private EventManagementListenerCallback eventManagementListenerCallback = new EventManagementListenerCallback() {
        @Override
        public void updateEvent(Event event) {

        }

        @Override
        public void returnCreateEvent(Event event) {

            if (event.isPasswordProtected) {
                //Save the password so repeated entries are possible
                SharedPreferences.Editor editor = getSharedPreferences(MainActivity.PREFERENCES, MODE_PRIVATE).edit();
                editor.putString(event.eventId, event.eventPassword);
                editor.apply();
            }

            String eventName = event.eventName;
            Intent intent = new Intent(CreateEventActivity.this, PlaylistActivity.class);
            intent.putExtra(PlaylistActivity.EVENT_NAME_EXTRA, eventName);
            intent.putExtra(PlaylistActivity.IS_HOST_EXTRA, true);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            CreateEventActivity.this.startActivity(intent);
        }

        @Override
        public void createEventNameAlreadyTaken() {
            Toast.makeText(CreateEventActivity.this, R.string.create_event_error_name_taken, Toast.LENGTH_SHORT).show();
            editTextPartyName.setText("");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        ButterKnife.bind(this);

        //Read location from activity intent
        latitude = getIntent().getDoubleExtra(LATITUDE_EXTRA, 0);
        longitude = getIntent().getDoubleExtra(LONGITUDE_EXTRA, 0);

        //Set action bar and fragment settings
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        eventSettingsFragment = (EventSettingsFragment) getSupportFragmentManager().findFragmentById(R.id.createEventSettingsFragment);

        Intent intent = new Intent(this, NetworkService.class);
        bindService(intent, serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                networkService = ((NetworkServiceBinder) service).networkService();
                eventManagementListener = new EventManagementListener(eventManagementListenerCallback);
                networkService.addListener(eventManagementListener);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                networkService.removeListener(eventManagementListener);
            }
        }, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (networkService != null) unbindService(serviceConnection);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    @OnClick(R.id.createEventFAB)
    public void createEvent() {

        double cooldown = 0;
        if (!editTextCooldown.getText().toString().isEmpty()) cooldown = Double.parseDouble(editTextCooldown.getText().toString()) * 1E3;

        double maxSongLength = 0;
        if (!editTextSongLength.getText().toString().isEmpty()) maxSongLength = Double.parseDouble(editTextSongLength.getText().toString()) * 1E3;

        String password = editTextPassword.getText().toString();
        String[] genres = eventSettingsFragment.selectedGenres();

        boolean isExplicitAllowed = switchExplicit.isChecked();
        boolean isDuplicatesAllowed = switchDuplicates.isChecked();

        if (!editTextPartyName.getText().toString().isEmpty()) {
            String eventName = editTextPartyName.getText().toString();
            String message = EventMessageBuilder.createEvent(eventName, password, latitude, longitude, cooldown, maxSongLength, isExplicitAllowed, isDuplicatesAllowed,genres);
            networkService.sendMessage(message);
        } else {
            Toast.makeText(networkService, emptyPartyNameToast, Toast.LENGTH_SHORT).show();
        }

    }
}
