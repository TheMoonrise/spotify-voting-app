package com.spotifyplaylistvoting.activities;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.gson.JsonObject;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.adapter.ListViewUserPlaylistsAdapter;
import com.spotifyplaylistvoting.helper.SpotifyAuth;
import com.spotifyplaylistvoting.networking.helper.NetworkServiceBinder;
import com.spotifyplaylistvoting.networking.interfaces.SongSearchListenerCallback;
import com.spotifyplaylistvoting.networking.json.EventMessageBuilder;
import com.spotifyplaylistvoting.networking.json.PlaylistMessageBuilder;
import com.spotifyplaylistvoting.networking.json.SongSearchListener;
import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.model.Track;
import com.spotifyplaylistvoting.networking.services.NetworkService;

import java.util.ArrayList;
import java.util.Arrays;

public class ImportPlaylistActivity extends AppCompatActivity {

    //View references
    @BindView(R.id.listViewResults) ListView resultsListView;
    private ListViewUserPlaylistsAdapter resultsAdapter;

    //The spotify auth helper
    private SpotifyAuth spotifyAuth;

    //The listener for messages from the server
    private SongSearchListener messageListener;

    //The connected network service
    private NetworkService networkService;
    private ServiceConnection serviceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_import_playlist);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spotifyAuth = new SpotifyAuth(this);
        spotifyAuth.authorizeSpotifyUser();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (resultsAdapter != null) resultsAdapter.cleanup();
        if (networkService != null) unbindService(serviceConnection);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SpotifyAuth.AUTHENTICATION_REQUEST_CODE:

                if (spotifyAuth.isRequestSuccessful(requestCode, resultCode, data)) {
                    Intent intent = new Intent(this, NetworkService.class);
                    bindService(intent, serviceConnection = new ServiceConnection() {
                        @Override
                        public void onServiceConnected(ComponentName name, IBinder service) {
                            networkService = ((NetworkServiceBinder) service).networkService();

                            //Create the list view adapter after the network service and auth key have been received
                            resultsAdapter = new ListViewUserPlaylistsAdapter(ImportPlaylistActivity.this, networkService, spotifyAuth.authKey(), new ArrayList<JsonObject>());
                            resultsListView.setAdapter(resultsAdapter);

                            //Attach a message listener to handle network callbacks
                            messageListener = new SongSearchListener(new SongSearchListenerCallback() {
                                @Override
                                public void updateEvent(Event event) {

                                }

                                @Override
                                public void returnGetHistory(Track[] history) {

                                }

                                @Override
                                public void returnGetPlaylist(Track[] playlist) {

                                }

                                @Override
                                public void returnAddTrack() {
                                    finish();
                                }

                                @Override
                                public void addTrackTrackNotAllowed() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ImportPlaylistActivity.this, R.string.import_activity_cannot_add_playlist_toast, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            });

                            networkService.addListener(messageListener);
                        }

                        @Override
                        public void onServiceDisconnected(ComponentName name) {
                            if (networkService != null) networkService.removeListener(messageListener);
                            networkService = null;
                        }
                    }, Context.BIND_AUTO_CREATE);

                } else {
                    Toast.makeText(this, R.string.search_error_toast, Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }
}
