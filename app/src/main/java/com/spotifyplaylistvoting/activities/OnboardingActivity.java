package com.spotifyplaylistvoting.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.spotifyplaylistvoting.R;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class OnboardingActivity extends AppCompatActivity {

    @BindView(R.id.onboardingViewPager) ViewPager viewPager;

    @BindView(R.id.onboardingFinish) View finishButton;
    @BindView(R.id.onboardingNext) View nextButton;

    @BindView(R.id.onboardingIdc0) ImageView indicator0;
    @BindView(R.id.onboardingIdc1) ImageView indicator1;
    @BindView(R.id.onboardingIdc2) ImageView indicator2;
    @BindView(R.id.onboardingIdc3) ImageView indicator3;

    @BindDrawable(R.drawable.ic_dot_selected) Drawable dotSelected;
    @BindDrawable(R.drawable.ic_dot_unselected) Drawable dotUnselected;

    private ImageView[] indicators;

    //The adapter for loading pager fragments
    private SectionsPagerAdapter sectionsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        ButterKnife.bind(this);

        indicators = new ImageView[]{indicator0, indicator1, indicator2, indicator3};

        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(sectionsPagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //Show and hide the indicators
                for (int i = 0; i < indicators.length; i++) indicators[i].setImageDrawable(i == position ? dotSelected : dotUnselected);

                //Show and hide the finish and next button
                finishButton.setVisibility(position == OnboardingFragment.PAGER_SIZE - 1 ? View.VISIBLE : View.INVISIBLE);
                nextButton.setVisibility(position == OnboardingFragment.PAGER_SIZE - 1 ? View.INVISIBLE : View.VISIBLE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) super.onBackPressed();
        else viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MainActivity.PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) switchToMainActivity();
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return OnboardingFragment.newOnbardingFragment(position);
        }

        @Override
        public int getCount() {
            return OnboardingFragment.PAGER_SIZE;
        }
    }

    @OnClick({R.id.onboardingFinish, R.id.onboardingSkip})
    public void switchToMainActivity() {
        //Request location permissions
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION},MainActivity.PERMISSIONS_REQUEST_CODE);
            return;
        }

        SharedPreferences.Editor editor = getSharedPreferences(MainActivity.PREFERENCES, MODE_PRIVATE).edit();
        editor.putBoolean(MainActivity.FIRST_START, false);
        editor.apply();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.onboardingNext)
    public void nextPage() {
        if (viewPager.getCurrentItem() < OnboardingFragment.PAGER_SIZE - 1) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
        }
    }
}
