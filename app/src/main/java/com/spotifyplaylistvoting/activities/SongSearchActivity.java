package com.spotifyplaylistvoting.activities;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.ListView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.gson.JsonObject;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.adapter.ListViewQueryResultsAdapter;
import com.spotifyplaylistvoting.helper.SpotifyAuth;
import com.spotifyplaylistvoting.networking.helper.NetworkServiceBinder;
import com.spotifyplaylistvoting.networking.interfaces.SongSearchListenerCallback;
import com.spotifyplaylistvoting.networking.json.EventMessageBuilder;
import com.spotifyplaylistvoting.networking.json.PlaylistMessageBuilder;
import com.spotifyplaylistvoting.networking.json.SongSearchListener;
import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.model.Track;
import com.spotifyplaylistvoting.networking.services.NetworkService;

import java.util.ArrayList;
import java.util.Arrays;

public class SongSearchActivity extends AppCompatActivity {

    //View references
    @BindView(R.id.listViewQueryResults) ListView resultsListView;
    private ListViewQueryResultsAdapter resultsAdapter;

    private SearchView searchView;

    //The spotify auth helper
    private SpotifyAuth spotifyAuth;

    //The complete playlist and history on the server
    private ArrayList<Track> playlist = new ArrayList<>();

    //The connected network service
    private NetworkService networkService;
    private ServiceConnection serviceConnection;

    //The listener for messages from the server
    private SongSearchListener messageListener = new SongSearchListener(new SongSearchListenerCallback() {
        @Override
        public void updateEvent(Event event) {
            resultsAdapter.updateEvent(event);
        }

        @Override
        public void returnGetHistory(Track[] tracks) {
            playlist.addAll(Arrays.asList(tracks));
        }

        @Override
        public void returnGetPlaylist(Track[] tracks) {
            playlist.addAll(Arrays.asList(tracks));
        }

        @Override
        public void returnAddTrack() {
            finish();
        }

        @Override
        public void addTrackTrackNotAllowed() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(SongSearchActivity.this, R.string.search_cannot_add_song_toast, Toast.LENGTH_SHORT).show();
                }
            });
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_song_search);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spotifyAuth = new SpotifyAuth(this);
        spotifyAuth.authorizeSpotifyUser();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menuSearchSong).getActionView();

        searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent.getAction().equals(Intent.ACTION_SEARCH)) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if (resultsAdapter != null) resultsAdapter.searchSppotify(query);
            Log.i("SpotifySearch", "user searched for " + query);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (resultsAdapter != null) resultsAdapter.cleanup();
        if (networkService != null) unbindService(serviceConnection);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SpotifyAuth.AUTHENTICATION_REQUEST_CODE:

                if (spotifyAuth.isRequestSuccessful(requestCode, resultCode, data)) {
                    searchView.setIconified(false);

                    Intent intent = new Intent(this, NetworkService.class);
                    bindService(intent, serviceConnection = new ServiceConnection() {
                        @Override
                        public void onServiceConnected(ComponentName name, IBinder service) {
                            networkService = ((NetworkServiceBinder) service).networkService();

                            //Create the list view adapter after the network service and auth key have been received
                            resultsAdapter = new ListViewQueryResultsAdapter(SongSearchActivity.this, networkService, spotifyAuth.authKey(), new ArrayList<JsonObject>(), playlist);
                            resultsListView.setAdapter(resultsAdapter);

                            //Attach a message listener to handle network callback
                            networkService.addListener(messageListener);

                            //Ask the server for the current event to properly filter songs received through search queries
                            networkService.sendMessage(EventMessageBuilder.getCurrentEvent());
                            networkService.sendMessage(PlaylistMessageBuilder.getHistory());
                            networkService.sendMessage(PlaylistMessageBuilder.getPlaylist());
                        }

                        @Override
                        public void onServiceDisconnected(ComponentName name) {
                            if (networkService != null) networkService.removeListener(messageListener);
                            networkService = null;
                        }
                    }, Context.BIND_AUTO_CREATE);

                } else {
                    Toast.makeText(this, R.string.search_error_toast, Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }
}
