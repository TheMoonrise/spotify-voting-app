package com.spotifyplaylistvoting.activities;

import android.content.*;
import android.os.IBinder;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Switch;
import butterknife.BindView;
import butterknife.OnClick;
import com.spotifyplaylistvoting.R;

import butterknife.ButterKnife;
import com.spotifyplaylistvoting.networking.helper.NetworkServiceBinder;
import com.spotifyplaylistvoting.networking.interfaces.EventManagementListenerCallback;
import com.spotifyplaylistvoting.networking.json.EventManagementListener;
import com.spotifyplaylistvoting.networking.json.EventMessageBuilder;
import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.services.NetworkService;

public class EditEventActivity extends AppCompatActivity {

    @BindView(R.id.eventSettingsEditTextSongLength) EditText editTextSongLength;
    @BindView(R.id.eventSettingsEditTextCooldown) EditText editTextCooldown;
    @BindView(R.id.eventSettingsExplicitSwitch) Switch switchExplicit;
    @BindView(R.id.eventSettingsDuplicateSwitch) Switch switchDuplicates;

    //Network service for server communication
    private NetworkService networkService;
    private ServiceConnection serviceConnection;
    private EventManagementListener eventManagementListener;

    //The event settings fragment
    private EventSettingsFragment eventSettingsFragment;

    //Listener callback for network events
    private EventManagementListenerCallback eventManagementListenerCallback = new EventManagementListenerCallback() {
        @Override
        public void updateEvent(final Event event) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    editTextSongLength.setText(event.maxSongLength > 0 ? String.valueOf((int)(event.maxSongLength / 1E3)) : "");
                    editTextCooldown.setText(event.addSongCooldown > 0 ? String.valueOf((int)(event.addSongCooldown / 1E3)) : "");
                    switchExplicit.setChecked(event.isExplicitAllowed);
                    switchDuplicates.setChecked(event.isDuplicateAllowed);

                    eventSettingsFragment.setSelectedGenres(event.bannedGenres);
                }
            });
        }

        @Override
        public void returnCreateEvent(Event event) {

        }

        @Override
        public void createEventNameAlreadyTaken() {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        eventSettingsFragment = (EventSettingsFragment) getSupportFragmentManager().findFragmentById(R.id.changeSettingsSettingsFragment);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, NetworkService.class);
        bindService(intent, serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                networkService = ((NetworkServiceBinder) service).networkService();
                eventManagementListener = new EventManagementListener(eventManagementListenerCallback);
                networkService.addListener(eventManagementListener);

                networkService.sendMessage(EventMessageBuilder.getCurrentEvent());
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                networkService.removeListener(eventManagementListener);
            }
        }, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (networkService != null) unbindService(serviceConnection);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.editEventFAB)
    public void editEvent() {

        if (networkService != null) {
            double cooldown = 0;
            if (!editTextCooldown.getText().toString().isEmpty()) cooldown = Double.parseDouble(editTextCooldown.getText().toString()) * 1E3;

            double maxSongLength = 0;
            if (!editTextSongLength.getText().toString().isEmpty()) maxSongLength = Double.parseDouble(editTextSongLength.getText().toString()) * 1E3;

            boolean isExplicitAllowed = switchExplicit.isChecked();
            boolean isDuplicateAllowed = switchDuplicates.isChecked();

            String[] genres = eventSettingsFragment.selectedGenres();

            String message = EventMessageBuilder.editEvent(cooldown, maxSongLength, isExplicitAllowed, isDuplicateAllowed, genres);
            networkService.sendMessage(message);
        }

        finish();
    }
}
