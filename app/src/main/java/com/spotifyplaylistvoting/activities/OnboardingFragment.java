package com.spotifyplaylistvoting.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.spotifyplaylistvoting.R;

public class OnboardingFragment extends Fragment {

    @BindView(R.id.onbardingImageView) ImageView imageView;
    @BindView(R.id.onbardingExplanation) TextView textView;

    @BindDrawable(R.drawable.onboarding_screen_01) Drawable screen01;
    @BindDrawable(R.drawable.onboarding_screen_02) Drawable screen02;
    @BindDrawable(R.drawable.onboarding_screen_03) Drawable screen03;
    @BindDrawable(R.drawable.onboarding_screen_04) Drawable screen04;

    @BindString(R.string.onboarding_explanation_01) String explanation01;
    @BindString(R.string.onboarding_explanation_02) String explanation02;
    @BindString(R.string.onboarding_explanation_03) String explanation03;
    @BindString(R.string.onboarding_explanation_04) String explanation04;


    //Argument for the section number of this fragment
    private static final String ARG_SECTION_NUMBER = "section number";

    //The total number of pages this pager can display
    public static final int PAGER_SIZE = 4;


    public static OnboardingFragment newOnbardingFragment(int sectionNumber) {
        OnboardingFragment fragment = new OnboardingFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_onboarding, container, false);

        if (getActivity() != null) {
            ButterKnife.bind(this, rootView);
            loadImage(getArguments().getInt(ARG_SECTION_NUMBER));
        }

        return rootView;
    }

    //Loads the specific image for this section number
    private void loadImage(int sectionNumber) {
        switch (sectionNumber) {
            case 0:
                imageView.setImageDrawable(screen01);
                textView.setText(explanation01);
                break;
            case 1:
                imageView.setImageDrawable(screen02);
                textView.setText(explanation02);
                break;
            case 2:
                imageView.setImageDrawable(screen03);
                textView.setText(explanation03);
                break;
            case 3:
                imageView.setImageDrawable(screen04);
                textView.setText(explanation04);
                break;
        }
    }
}
