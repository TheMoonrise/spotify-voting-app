package com.spotifyplaylistvoting.activities;


import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.*;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.adapter.GenresRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.Arrays;

//Fragment handling the display of info dialogues for event settings
public class EventSettingsFragment extends Fragment {

    //Bind layout views
    @BindView(R.id.eventSettingsSelectedGenres) RecyclerView genresRecyclerView;
    @BindView(R.id.eventSettingsEditTextGenres) EditText genresSearchBar;

    //Dialog messages for info events
    @BindString(R.string.dialog_accept) String dialogAccept;

    @BindString(R.string.create_event_cooldown_name) String cooldownDialogTitle;
    @BindString(R.string.cooldown_dialog_message) String cooldownDialogMessage;
    @BindString(R.string.create_event_duplicate_name) String duplicateDialogTitle;
    @BindString(R.string.duplicate_dialog_message) String duplicateDialogMessage;
    @BindString(R.string.create_event_explicit_name) String explicitDialogTitle;
    @BindString(R.string.explicit_dialog_message) String explicitDialogMessage;
    @BindString(R.string.create_event_song_length_name) String songLengthDialogTitle;
    @BindString(R.string.song_length_dialog_message) String songLengthDialogMessage;
    @BindString(R.string.create_event_genre_name) String genresDialogTitle;
    @BindString(R.string.genre_dialog_message) String genresDialogMessage;

    //The array of genre string
    @BindArray(R.array.songGenres) String[] genres;

    //Constants for genre searches
    private final int MAX_GENRES_DISPLAYED = 8, BEGINNING_SEARCH_LENGTH = 2;

    //Lists of visible and selected genres
    private ArrayList<String> visibleGenres = new ArrayList<>();
    private ArrayList<String> selectedGenres = new ArrayList<>();

    //The recycler view adapter
    GenresRecyclerViewAdapter recyclerViewAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_settings, container, false);
        ButterKnife.bind(this, view);

        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);

        recyclerViewAdapter = new GenresRecyclerViewAdapter(visibleGenres, selectedGenres, getContext());

        genresRecyclerView.setLayoutManager(layoutManager);
        genresRecyclerView.setAdapter(recyclerViewAdapter);
        genresRecyclerView.setNestedScrollingEnabled(false);

        genresSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchGenres(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    //Creates a info dialog
    private void showInfoDialog(String title, String message) {
        if (getActivity() == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(title);
        builder.setMessage(message);

        // Add the ok button
        builder.setPositiveButton(dialogAccept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        builder.create().show();
    }

    //Returns the selected genres
    public String[] selectedGenres() {
        return selectedGenres.toArray(new String[0]);
    }

    //Allows to preset the selected genres
    public void setSelectedGenres(String[] selectedGenres) {
        this.selectedGenres.clear();
        this.selectedGenres.addAll(Arrays.asList(selectedGenres));

        if (genresSearchBar.getText().toString().isEmpty()) {
            this.visibleGenres.clear();
            this.visibleGenres.addAll(Arrays.asList(selectedGenres));
            recyclerViewAdapter.notifyDataSetChanged();
        }
    }

    //Updates the genres displayed depending on the search bar content
    private void searchGenres(String query) {
        visibleGenres.clear();
        query = query.trim();

        if (query.length() > 0) {

            //Search via substring matching when the query is longer than the threshold
            //Otherwise mathe the first letters of the query
            if (query.length() > BEGINNING_SEARCH_LENGTH) {
                String[] queryParts = query.toLowerCase().split(" ");

                for (String genre : genres) {
                    boolean isMatching = true;
                    for (String q : queryParts) if (!genre.contains(q)) isMatching = false;

                    if (isMatching) {
                        visibleGenres.add(genre);
                        if (visibleGenres.size() >= MAX_GENRES_DISPLAYED) break;
                    }
                }

            } else {
                for (String genre : genres) {
                    if (query.equals(genre.substring(0, query.length()))) {
                        visibleGenres.add(genre);
                        if (visibleGenres.size() >= MAX_GENRES_DISPLAYED) break;
                    }
                }
            }

        } else {
            visibleGenres.addAll(selectedGenres);
        }

        recyclerViewAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.eventSettingsDuplicateInfo)
    public void createDuplicateDialog() {
        showInfoDialog(duplicateDialogTitle, duplicateDialogMessage);
    }

    @OnClick(R.id.eventSettingsExplicitInfo)
    public void createExplicitDialog() {
        showInfoDialog(explicitDialogTitle, explicitDialogMessage);
    }

    @OnClick(R.id.eventSettingsCooldownInfo)
    public void createCooldownDialog() {
        showInfoDialog(cooldownDialogTitle, cooldownDialogMessage);
    }

    @OnClick(R.id.eventSettingsSongLengthInfo)
    public void createSongLengthDialog() {
        showInfoDialog(songLengthDialogTitle, songLengthDialogMessage);
    }

    @OnClick(R.id.eventSettingsGenreInfo)
    public void createGenreDialog() {
        showInfoDialog(genresDialogTitle, genresDialogMessage);
    }
}
