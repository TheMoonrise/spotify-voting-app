package com.spotifyplaylistvoting.activities;

import android.content.*;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import butterknife.BindAnim;
import butterknife.OnClick;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.adapter.PlaylistRecyclerViewAdapter;
import com.spotifyplaylistvoting.helper.ShareURL;
import com.spotifyplaylistvoting.networking.helper.NetworkServiceBinder;
import com.spotifyplaylistvoting.networking.interfaces.PlaylistListenerCallback;
import com.spotifyplaylistvoting.networking.json.EventMessageBuilder;
import com.spotifyplaylistvoting.networking.json.PlaylistListener;
import com.spotifyplaylistvoting.networking.json.PlaylistMessageBuilder;
import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.model.Track;
import com.spotifyplaylistvoting.networking.services.NetworkService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.spotifyplaylistvoting.networking.services.SpotifyHostService;

public class PlaylistActivity extends AppCompatActivity {

    //@BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.playlistRecyclerView) RecyclerView songRecyclerView;
    @BindView(R.id.playlistStartPlayback) CardView startPlaybackView;
    @BindView(R.id.playlistImportTaunt) View importButton;

    @BindView(R.id.playlistFAB) FloatingActionButton fab;
    @BindView(R.id.playlistFABTextView) TextView fabTextView;
    @BindView(R.id.playlistFABPlus) ImageView fabPlus;


    @BindString(R.string.endParty_dialog_title) String endEventDialogTitle;
    @BindString(R.string.endParty_dialog_message) String endEventDialogMessage;
    @BindString(R.string.dialog_accept) String dialogAccept;
    @BindString(R.string.dialog_cancel) String dialogCancel;

    @BindAnim(R.anim.plus_pulsing) Animation animationPlus;

    //Keys for delivered intent Extras
    public static final String EVENT_NAME_EXTRA = "Playlist Title";
    public static final String IS_HOST_EXTRA = "Is Host";

    //The cooldown unlocking the add button
    private CountDownTimer countDownTimer;

    //Stores whether this client is host of the current event
    private boolean isHost;

    //The current event
    private Event event;

    //The name of the current event
    private String eventName;

    private LinearLayoutManager linearLayoutManager;
    private PlaylistRecyclerViewAdapter playlistRecyclerViewAdapter;

    //The track in the current playlist
    private List<Track> tracks = new ArrayList<>();

    //Components for server communication
    private NetworkService networkService;
    private PlaylistListener playlistListener;
    private ServiceConnection serviceConnection;

    //The broadcast manager to receive updates on the service state
    private LocalBroadcastManager broadcastManager;


    //The local broadcast receiver to handle broadcasts from services
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra(SpotifyHostService.PLAYLIST_ACTIVE_EXTRA, true)) startPlaybackView.setVisibility(View.GONE);
            else startPlaybackView.setVisibility(View.VISIBLE);
        }
    };

    //Create the listener callback to handle incoming server messages
    private PlaylistListenerCallback playlistListenerCallback = new PlaylistListenerCallback() {
        @Override
        public void updateEvent(Event event) {
            PlaylistActivity.this.event = event;
        }

        @Override
        public void returnGetPlaylist(final Track[] playlist) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tracks.clear();
                    tracks.addAll(Arrays.asList(playlist));
                    playlistRecyclerViewAdapter.notifyDataSetChanged();

                    updateImportButton();
                }
            });
        }

        @Override
        public void updateTrack(final Track track) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final int index = trackWithIdIndex(track.trackId);
                    if (index == -1 ) return;

                    Log.d("DEBUG", "index: " + index + " position " + track.position);
                    tracks.remove(index);
                    tracks.add(track.position, track);

                    if (index != track.position) playlistRecyclerViewAdapter.notifyItemMoved(index, track.position);
                    playlistRecyclerViewAdapter.notifyItemChanged(track.position);
                    updateImportButton();
                }
            });
        }

        @Override
        public void addTrack(final Track track) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tracks.add(track.position, track);

                    playlistRecyclerViewAdapter.notifyItemInserted(track.position);
                    updateImportButton();
                }
            });
        }

        @Override
        public void removeTrack(final String trackId) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final int index = trackWithIdIndex(trackId);
                    if (index == -1 ) return;

                    tracks.remove(index);

                    playlistRecyclerViewAdapter.notifyItemRemoved(index);
                    updateImportButton();
                }
            });
        }

        @Override
        public void returnGetSongAddCooldown(final long lastVote, final long cooldown) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    startCooldownCountdown(lastVote, cooldown);
                }
            });
        }

        //Close the activity when the event was ended
        @Override
        public void eventEnded() {
            finish();
            Toast.makeText(PlaylistActivity.this, R.string.playlist_event_end_toast, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void eventNotAvailable() {
            finish();
        }

        @Override
        public void returnGetHistory(Track[] playlist) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        eventName = intent.getStringExtra(EVENT_NAME_EXTRA);
        setTitle(eventName);

        isHost = intent.getBooleanExtra(IS_HOST_EXTRA, false);
        if (!isHost || SpotifyHostService.IS_PLAYING) startPlaybackView.setVisibility(View.GONE);

        broadcastManager = LocalBroadcastManager.getInstance(this);
        broadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(SpotifyHostService.BROADCAST_EVENT_NAME));

        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        songRecyclerView.setLayoutManager(linearLayoutManager);
        songRecyclerView.setItemAnimator(new DefaultItemAnimator());

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                networkService = ((NetworkServiceBinder) service).networkService();
                playlistListener = new PlaylistListener(playlistListenerCallback);
                networkService.addListener(playlistListener);

                playlistRecyclerViewAdapter = new PlaylistRecyclerViewAdapter(PlaylistActivity.this, tracks, networkService);
                songRecyclerView.setAdapter(playlistRecyclerViewAdapter);

                networkService.sendMessage(PlaylistMessageBuilder.getPlaylist());
                networkService.sendMessage(PlaylistMessageBuilder.getSongAddCooldown());
                networkService.sendMessage(EventMessageBuilder.getCurrentEvent());
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                if (networkService != null) networkService.removeListener(playlistListener);
            }
        };

        Intent networkIntent = new Intent(this, NetworkService.class);
        bindService(networkIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        broadcastManager.unregisterReceiver(broadcastReceiver);
        if (networkService != null) unbindService(serviceConnection);
        if (countDownTimer != null) countDownTimer.cancel();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(networkService != null) {
            networkService.sendMessage(PlaylistMessageBuilder.getPlaylist());
            networkService.sendMessage(PlaylistMessageBuilder.getSongAddCooldown());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isHost) {
            getMenuInflater().inflate(R.menu.playlist, menu);
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuPlaylistSettings:
                Intent intent = new Intent(this, EditEventActivity.class);
                startActivity(intent);
                return true;
            case R.id.menuPlaylistPlay:
                startPlayback();
                return true;
            case R.id.menuSharePlaylist:
                //Create a share url for the current event
                if (event == null) return false;
                ShareURL shareURL = new ShareURL(event.eventId, event.eventPassword);
                String shareMessage = getResources().getString(R.string.playlist_share_intent_value, shareURL.toString());
                Log.d("PlaylistActivity", "Sharing message for event " + event.eventId + " " + event.eventPassword + " with " + shareMessage);

                //Send the share intent request
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.playlist_share_intent_title)));
                return true;
            case R.id.menuEndEvent:
                showEndEventDialog();
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Finds a song in the playlist with the provided id
    //The index will match the track in the tracks list
    //If no match is found -1 will be returned
    private int trackWithIdIndex(String id) {
        for (int i = 0; i < tracks.size(); i++) if (tracks.get(i).trackId.equals(id)) return i;
        return -1;
    }

    private void showEndEventDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(endEventDialogTitle);
        builder.setMessage(endEventDialogMessage);

        // Add the ok button
        builder.setPositiveButton(dialogAccept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                networkService.sendMessage(PlaylistMessageBuilder.endEvent());
            }
        });

        //Add the cancel button
        builder.setNegativeButton(dialogCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.create().show();
    }

    //Updates the visibility of the import message
    private void updateImportButton() {
        if (playlistRecyclerViewAdapter.getItemCount() > 0 || !isHost) importButton.setVisibility(View.GONE);
        else importButton.setVisibility(View.VISIBLE);
    }

    //Starts a countdown for unlocking the add song fab
    private void startCooldownCountdown(final long lastVote, final long cooldown) {

        if (countDownTimer != null) countDownTimer.cancel();
        long duration = cooldown - (System.currentTimeMillis() - lastVote);

        if (duration > 0) {
            fab.setClickable(false);
            fabPlus.clearAnimation();
            fabTextView.setVisibility(View.VISIBLE);
            fabPlus.setVisibility(View.INVISIBLE);

            countDownTimer = new CountDownTimer(duration, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    int remainder = (int)((cooldown - (System.currentTimeMillis() - lastVote)) / 1000);
                    fabTextView.setText(remainder + getResources().getString(R.string.main_unit_seconds));
                }

                @Override
                public void onFinish() {
                    fab.setClickable(true);
                    fabTextView.setVisibility(View.INVISIBLE);
                    fabPlus.setVisibility(View.VISIBLE);
                    fabPlus.setAnimation(animationPlus);
                }
            }.start();

        } else {
            fab.setClickable(true);
            fabTextView.setVisibility(View.INVISIBLE);
            fabPlus.setVisibility(View.VISIBLE);
        }
    }

    //Opens the song search activity
    @OnClick(R.id.playlistFAB)
    public void searchSong() {
        Intent intent = new Intent(this, SongSearchActivity.class);
        startActivity(intent);
    }

    //Opens the import playlist activity
    @OnClick(R.id.playlistImportTaunt)
    public void importPlaylist() {
        Intent intent = new Intent(this, ImportPlaylistActivity.class);
        startActivity(intent);
    }

    //Begins the playback of the playlist
    @OnClick(R.id.playlistStartPlayback)
    public void startPlayback() {
        Intent intent = new Intent(this, SpotifyHostService.class);
        intent.putExtra(SpotifyHostService.EVENT_NAME_EXTRA, eventName);
        startService(intent);
    }
}
