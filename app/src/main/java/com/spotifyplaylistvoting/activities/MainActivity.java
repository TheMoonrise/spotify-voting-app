package com.spotifyplaylistvoting.activities;

import android.content.*;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.*;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.google.android.gms.tasks.OnSuccessListener;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.adapter.EventsRecyclerViewAdapter;
import com.spotifyplaylistvoting.helper.ShareURL;
import com.spotifyplaylistvoting.networking.helper.NetworkServiceBinder;
import com.spotifyplaylistvoting.networking.interfaces.MenuListenerCallback;
import com.spotifyplaylistvoting.networking.interfaces.NetworkStateListener;
import com.spotifyplaylistvoting.networking.json.EventMessageBuilder;
import com.spotifyplaylistvoting.networking.json.MenuListener;
import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.services.NetworkService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends AppCompatActivity{

    @BindView(R.id.appLogo) ImageView appLogo;
    @BindView(R.id.mainActivityecyclerView) RecyclerView recyclerView;
    @BindView(R.id.mainActivityFAB) FloatingActionButton fab;
    @BindView(R.id.mainRefresh) SwipeRefreshLayout refreshLayout;

    //Shared preference key for saving event passwords
    public static final String PREFERENCES = "Preferences", FIRST_START = "First Start";

    //Request code for permission request
    public static final int PERMISSIONS_REQUEST_CODE = 5578, RESOLUTION_REQUEST_CODE = 2234;

    //All current events
    private List<Event> allEvents = new ArrayList<>();

    private LinearLayoutManager linearLayoutManager;
    private EventsRecyclerViewAdapter eventsRecyclerViewAdapter;


    private NetworkService networkService;
    private MenuListener menuListener;
    private ServiceConnection serviceConnection;

    // For accessing user location
    private FusedLocationProviderClient locationClient;
    private LocationCallback locationCallback;
    private Location location;

    private NetworkStateListener networkStateListener = new NetworkStateListener() {
        @Override
        public void onConnectionOpened() {
            refreshEvents();
        }

        @Override
        public void onConnectionClosed() {

        }
    };

    private MenuListenerCallback menuListenerCallback = new MenuListenerCallback() {
        @Override
        public void returnGetNearbyEvents(final Event[] events) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    allEvents.clear();
                    allEvents.addAll(Arrays.asList(events));

                    eventsRecyclerViewAdapter.notifyDataSetChanged();
                    refreshLayout.setRefreshing(false);
                }
            });
        }

        @Override
        public void returnJoinEvent(Event event, boolean isHost) {
            if (event.isPasswordProtected) {
                //Save the password so repeated entries are possible
                SharedPreferences.Editor editor = getSharedPreferences(PREFERENCES, MODE_PRIVATE).edit();
                editor.putString(event.eventId, event.eventPassword);
                editor.apply();
            }

            String eventName = event.eventName;
            Intent intent = new Intent(MainActivity.this, PlaylistActivity.class);
            intent.putExtra(PlaylistActivity.EVENT_NAME_EXTRA, eventName);
            intent.putExtra(PlaylistActivity.IS_HOST_EXTRA, isHost);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }

        @Override
        public void joinEventIncorrectPassword() {
            Toast.makeText(MainActivity.this, getString(R.string.main_incorrect_password_toast), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void joinEventEventNotAvailable() {
            refreshEvents();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (isFirstStart()) {
            finish();
            return;
        }

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //Access the location client
        requestLocationUpdate();
        locationClient = LocationServices.getFusedLocationProviderClient(this);

        //Set the swipe refresh layout listener
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshEvents();
                if (location == null) refreshLayout.setRefreshing(false);
            }
        });

        linearLayoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);

        Intent intent = new Intent(this, NetworkService.class);
        bindService(intent, serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                networkService = ((NetworkServiceBinder) service).networkService();
                eventsRecyclerViewAdapter = new EventsRecyclerViewAdapter(allEvents, MainActivity.this, networkService);
                recyclerView.setAdapter(eventsRecyclerViewAdapter);

                menuListener = new MenuListener(menuListenerCallback);
                networkService.addListener(menuListener);
                networkService.addListener(networkStateListener);

                jointIntentEvent();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                if (networkService != null) {
                    networkService.removeListener(menuListener);
                    networkService.removeListener(networkStateListener);
                }
            }
        }, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        refreshEvents();
        requestLocationUpdate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (networkService != null) unbindService(serviceConnection);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestLocationUpdate();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RESOLUTION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) requestLocationUpdate();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        jointIntentEvent();
    }

    //Checks if this is the first start of the application
    private boolean isFirstStart() {
        SharedPreferences preferences = this.getSharedPreferences(MainActivity.PREFERENCES, Context.MODE_PRIVATE);
        boolean isFirstStart = preferences.getBoolean(FIRST_START, true);

        if (isFirstStart) {
            Intent intent = new Intent(this, OnboardingActivity.class);
            startActivity(intent);
        }

        return isFirstStart;
    }

    //Joins the event provided in the intent
    private void jointIntentEvent() {
        Uri uri = getIntent().getData();
        if (uri == null) return;

        ShareURL shareURL = new ShareURL(uri);

        if (shareURL.isValid()) {
            String message = EventMessageBuilder.joinEvent(shareURL.id(), shareURL.password());
            networkService.sendMessage(message);
        }
    }

    //Calls the server to refresh the list of events
    private void refreshEvents() {

        if (isLocationUnavailable()) return;

        String message = EventMessageBuilder.getNearbyEvents(location.getLatitude(), location.getLongitude());

        if (networkService != null) {
            networkService.sendMessage(message);
            refreshLayout.setRefreshing(true);
        }
    }

    //Checks whether a valid location is available
    private boolean isLocationUnavailable(){
        if (location != null) return false;
        requestLocationUpdate();
        return true;
    }

    //Requests a location update and asks the user to enable the required settings
    private void requestLocationUpdate() {
        //Request location permissions
        if (ActivityCompat.checkSelfPermission(MainActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION},PERMISSIONS_REQUEST_CODE);
            return;
        }

        //Build a location request to receive a one time location update
        final LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setNumUpdates(1);
        locationRequest.setInterval(0);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        //Check if the device settings satisfy the location request
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        //Request a location update if the settings meet the requirements
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationClient.requestLocationUpdates(locationRequest, new LocationCallback(){
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            if (locationResult != null) {
                                location = locationResult.getLastLocation();
                                refreshEvents();
                            }
                        }
                    }, null);
                }
            }
        });

        //Show a prompt to change device settings if the requirements are not met
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(MainActivity.this, RESOLUTION_REQUEST_CODE);

                    } catch (IntentSender.SendIntentException ignored) {

                    }
                }
            }
        });
    }

    @OnClick(R.id.mainActivityFAB)
    public void startCreatePartyActivity(View v) {

        if (isLocationUnavailable()) return;

        Intent intent = new Intent(MainActivity.this, CreateEventActivity.class);
        intent.putExtra(CreateEventActivity.LATITUDE_EXTRA, location.getLatitude());
        intent.putExtra(CreateEventActivity.LONGITUDE_EXTRA, location.getLongitude());
        MainActivity.this.startActivity(intent);
    }
}
