package com.spotifyplaylistvoting.adapter;

import android.content.Context;
import android.util.Log;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.volley.toolbox.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.helper.VolleySpotifyQueue;
import com.spotifyplaylistvoting.networking.json.PlaylistMessageBuilder;
import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.model.Track;
import com.spotifyplaylistvoting.networking.services.NetworkService;

import java.util.ArrayList;

//Displays the result of search queries to the spotify web api
public class ListViewQueryResultsAdapter extends ArrayAdapter<JsonObject> {

    //Layout properties to be bound to
    @BindView(R.id.songCardView) public CardView cardView;
    @BindView(R.id.imageViewSearchCoverArt) public NetworkImageView coverArt;
    @BindView(R.id.playlistTextSearchTitle) public TextView title;
    @BindView(R.id.textSearchSubtitle) public TextView artists;

    //Spotify urls
    private static final String SEARCH_URL = "https://api.spotify.com/v1/search?q=";
    private static final String ARTISTS_URL = "https://api.spotify.com/v1/artists?ids=";

    //All songs in the current playlist and history
    private ArrayList<Track> playlist;

    //The list holding the json objects to be displayed
    private ArrayList<JsonObject> tracksJson;

    //The network service to forward results to
    private NetworkService networkService;

    //The event representation to add songs to
    private Event event;

    //The context of the attached activity
    private Context context;

    //Request queue for handling server requests
    private VolleySpotifyQueue spotifyQueue;


    public ListViewQueryResultsAdapter(Context context, NetworkService networkService, String spotifyAuthKey, ArrayList<JsonObject> tracksJson, ArrayList<Track> playlist) {
        super(context, 0, tracksJson);

        this.networkService = networkService;
        this.tracksJson = tracksJson;
        this.playlist = playlist;
        this.context = context;

        spotifyQueue = new VolleySpotifyQueue(context, spotifyAuthKey);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView != null ? convertView : LayoutInflater.from(context).inflate(R.layout.search_list_entry, parent, false);
        ButterKnife.bind(this, view);

        JsonObject json = tracksJson.get(position);

        //Read the song title
        final String titleString = json.get("name").getAsString();
        title.setText(titleString);

        //Read the subtitle of this song
        StringBuilder artistsBuilder = new StringBuilder();
        final ArrayList<String> artistsList = new ArrayList<>();

        for (JsonElement a : json.getAsJsonArray("artists")) {
            String name = a.getAsJsonObject().get("name").getAsString();
            artistsBuilder.append(name).append(" • ");
            artistsList.add(name);
        }
        artists.setText(artistsBuilder.substring(0, artistsBuilder.length() - 3));

        //Reade the url for the cover image
        JsonArray images = json.getAsJsonObject("album").getAsJsonArray("images");
        String coverImageString = "";

        if (images.size() >= 2) coverImageString = images.get(2).getAsJsonObject().get("url").getAsString();
        else if (images.size() > 0) coverImageString = images.get(images.size() - 1).getAsJsonObject().get("url").getAsString();

        coverArt.setDefaultImageResId(R.drawable.default_cover_art);
        coverArt.setImageUrl(coverImageString, spotifyQueue.imageLoader());


        //Read additional properties of the song
        final String uriString = json.get("uri").getAsString();
        final long duration = json.get("duration_ms").getAsLong();
        final boolean isExplicit = json.get("explicit").getAsBoolean();

        String finalCoverImageString = coverImageString;
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = PlaylistMessageBuilder.addTrack(titleString, uriString, finalCoverImageString, artistsList.toArray(new String[0]), duration, isExplicit);
                networkService.sendMessage(message);
            }
        });

        return view;
    }

    //Sets the properties of the event for filtering
    public void updateEvent(Event event) {
        this.event = event;
    }

    //Cancels all ongoing http requests and unregister any listeners
    public void cleanup() {
        spotifyQueue.cancelRequests();
    }

    //Filters the current tracks by the limitations of the underlying event
    private boolean trackAllowed(JsonObject track) {

        if (event.maxSongLength > 0 && track.get("duration_ms").getAsLong() > event.maxSongLength) return false;
        if (track.get("explicit").getAsBoolean() && !event.isExplicitAllowed) return false;

        if (!event.isDuplicateAllowed) {
            String uri = track.get("uri").getAsString();
            for (Track t : playlist) if (t.trackUri.equals(uri)) return false;
        }
        return true;
    }

    //Takes the result of an artistsTextView query and uses it to filter search results
    private void handleArtistsResponse(String response) {
        JsonParser parser = new JsonParser();
        JsonArray artists = parser.parse(response).getAsJsonObject().getAsJsonArray("artists");

        ArrayList<String> bannedArtists = new ArrayList<>();

        for (JsonElement j : artists) {
            JsonObject artist = j.getAsJsonObject();
            for (JsonElement jj : artist.getAsJsonArray("genres")) {

                boolean isBanned = false;
                for (String g : event.bannedGenres) {
                    if (g.equals(jj.getAsString())) isBanned = true;
                }

                if (isBanned) {
                    bannedArtists.add(artist.get("id").getAsString());
                    break;
                }
            }
        }

        for (int i = 0; i < tracksJson.size(); i++) {
            JsonObject track = tracksJson.get(i);
            for (JsonElement j : track.getAsJsonArray("artists")) {
                if (bannedArtists.contains(j.getAsJsonObject().get("id").getAsString())) {
                    tracksJson.remove(i--);
                    break;
                }
            }
        }
        notifyDataSetChanged();
    }

    //Takes a query result and transforms it into readable json objects
    public void handleQueryResponse(String response) {
        if (event == null) return;

        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(response).getAsJsonObject();
        JsonArray tracks = json.getAsJsonObject("tracks").getAsJsonArray("items");

        tracksJson.clear();
        StringBuilder artists = new StringBuilder();

        for (JsonElement j : tracks) {
            JsonObject track = j.getAsJsonObject();
            if (trackAllowed(track)) {
                tracksJson.add(track);
                for (JsonElement jj : track.getAsJsonArray("artists")) artists.append(jj.getAsJsonObject().get("id").getAsString()).append(",");
            }
        }

        if (artists.length() > 0) {
            spotifyQueue.sendRequest(spotifyQueue.url(ARTISTS_URL, artists.substring(0, artists.length() - 1)), new VolleySpotifyQueue.ResponseListener() {
                @Override
                public void onResponse(String response) {
                    handleArtistsResponse(response);
                }
            });

        } else {
            notifyDataSetChanged();
        }
    }

    //Accepts search requests
    public void searchSppotify(String query) {
        String url = spotifyQueue.url(SEARCH_URL, query, "&type=track");
        spotifyQueue.sendRequest(url, new VolleySpotifyQueue.ResponseListener() {
            @Override
            public void onResponse(String response) {
                handleQueryResponse(response);
            }
        });
    }
}
