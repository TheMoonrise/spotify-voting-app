package com.spotifyplaylistvoting.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.spotifyplaylistvoting.R;

import java.util.List;

public class GenresRecyclerViewAdapter extends RecyclerView.Adapter<GenresRecyclerViewAdapter.GenreViewHolder> {

    //List of all genres seleced or displayed
    private List<String> visibleGenres;
    private List<String> selectedGenres;

    //The activity context
    private Context context;

    public static class GenreViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.eventSettingsGenreTextView) TextView genreName;
        @BindView(R.id.eventSettingsGenreIcon) ImageView actionIcon;
        LinearLayout backgroundView;

        GenreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            backgroundView = (LinearLayout) itemView;
        }
    }

    public GenresRecyclerViewAdapter(List<String> visibleGenres, List<String> selectedGenres, Context context) {
        this.visibleGenres = visibleGenres;
        this.selectedGenres = selectedGenres;
        this.context = context;
    }

    @NonNull
    @Override
    public GenreViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.genre_view, viewGroup, false);
        return new GenreViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final GenreViewHolder genreViewHolder, int i) {
        final String genre = visibleGenres.get(i);
        genreViewHolder.genreName.setText(genre);

        if (selectedGenres.contains(genre)) {
            genreViewHolder.actionIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_check));
            genreViewHolder.backgroundView.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));

            genreViewHolder.backgroundView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedGenres.remove(genre);
                    notifyItemChanged(genreViewHolder.getAdapterPosition());
                }
            });

        } else {
            genreViewHolder.actionIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_plus));
            genreViewHolder.backgroundView.setBackgroundColor(context.getResources().getColor(R.color.backgroundGrayscale));

            genreViewHolder.backgroundView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedGenres.add(genre);
                    notifyItemChanged(genreViewHolder.getAdapterPosition());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return visibleGenres.size();
    }
}
