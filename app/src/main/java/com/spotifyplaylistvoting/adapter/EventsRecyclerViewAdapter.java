package com.spotifyplaylistvoting.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import butterknife.BindString;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.activities.MainActivity;
import com.spotifyplaylistvoting.networking.json.EventMessageBuilder;
import com.spotifyplaylistvoting.networking.model.Event;
import com.spotifyplaylistvoting.networking.services.NetworkService;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventsRecyclerViewAdapter extends RecyclerView.Adapter<EventsRecyclerViewAdapter.EventViewHolder> {

    //List of all current events
    private List<Event> events;

    //The activity context
    private Context context;

    //The connected network service
    private NetworkService networkService;

    public static class EventViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mainAdditionalInfo) LinearLayout additionalInfo;

        @BindView(R.id.mainEventCardView) CardView eventCardView;
        @BindView(R.id.mainIconLockClosed) ImageView lockClosed;
        @BindView(R.id.mainIconLockOpen) ImageView lockOpen;
        @BindView(R.id.mainExpandArrow) ImageView expand;

        @BindView(R.id.mainTextViewEventName) TextView eventName;
        @BindView(R.id.mainTextViewDuplicates) TextView duplicates;
        @BindView(R.id.mainTextViewExplicit) TextView explicit;
        @BindView(R.id.mainTextViewCooldown) TextView cooldown;
        @BindView(R.id.mainTextViewSongLength) TextView songLength;
        @BindView(R.id.mainTextViewGenres) TextView genres;
        @BindView(R.id.mainTitleGenres) TextView geresTitle;

        @BindString(R.string.main_value_true) String trueString;
        @BindString(R.string.main_value_false) String falseString;
        @BindString(R.string.main_unit_seconds) String timeUnit;

        EventViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

    public EventsRecyclerViewAdapter(List<Event> events, Context context, NetworkService networkService) {
        this.events = events;
        this.context = context;
        this.networkService = networkService;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_card_view, viewGroup, false);
        EventViewHolder eventViewHolder = new EventViewHolder(v);
        return eventViewHolder;
    }

    @Override
    public void onBindViewHolder(final EventViewHolder viewHolder, int i) {
        final Event event = events.get(i);

        //Fill the view with the event information
        viewHolder.eventName.setText(event.eventName);

        //Show and hide the lock icons
        if (event.isPasswordProtected) {
            viewHolder.lockOpen.setVisibility(View.GONE);
            viewHolder.lockClosed.setVisibility(View.VISIBLE);

        } else {
            viewHolder.lockOpen.setVisibility(View.VISIBLE);
            viewHolder.lockClosed.setVisibility(View.GONE);
        }

        //Fill in additional information fields
        viewHolder.duplicates.setText(event.isDuplicateAllowed ? viewHolder.trueString : viewHolder.falseString);
        viewHolder.explicit.setText(event.isExplicitAllowed ? viewHolder.trueString : viewHolder.falseString);

        String cooldown = (int)(event.addSongCooldown / 1E3) + viewHolder.timeUnit;
        viewHolder.cooldown.setText(cooldown);
        viewHolder.songLength.setText(event.maxSongLength > 0 ? (int)(event.maxSongLength  / 1E3) + viewHolder.timeUnit : "∞");

        //Display the forbidden genres
        if (event.bannedGenres.length == 0) {
            viewHolder.geresTitle.setVisibility(View.GONE);
            viewHolder.genres.setVisibility(View.GONE);

        } else {
            viewHolder.geresTitle.setVisibility(View.VISIBLE);
            viewHolder.genres.setVisibility(View.VISIBLE);

            StringBuilder genresStringBuilder = new StringBuilder();
            for (String g : event.bannedGenres) genresStringBuilder.append(g).append(" • ");
            viewHolder.genres.setText(genresStringBuilder.substring(0, genresStringBuilder.length() - 3));
        }

        //Add on click listeners to the card and the expansion arrow
        viewHolder.eventCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = context.getSharedPreferences(MainActivity.PREFERENCES, Context.MODE_PRIVATE);
                String password = preferences.getString(event.eventId, "");

                if (event.isPasswordProtected && password.equals("")) createPasswordDialog(event);
                else networkService.sendMessage(EventMessageBuilder.joinEvent(event.eventId, password));
            }
        });

        viewHolder.expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.additionalInfo.getVisibility() == View.GONE) {
                    viewHolder.additionalInfo.setVisibility(View.VISIBLE);
                    viewHolder.expand.animate().rotationBy(180).setDuration(300).start();

                } else {
                    viewHolder.additionalInfo.setVisibility(View.GONE);
                    viewHolder.expand.animate().rotationBy(-180).setDuration(300).start();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void createPasswordDialog(Event event) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(event.eventName);
        builder.setMessage(R.string.join_private_event_dialog_message);

        //Inflate the custom dialog view
        View passwordView = LayoutInflater.from(context).inflate(R.layout.password_dialog, null);
        builder.setView(passwordView);

        final EditText editTextPassword = passwordView.findViewById(R.id.passwordDialogEnterPassword);
        final String eventId = event.eventId;

        // Add the ok button
        builder.setPositiveButton(context.getString(R.string.dialog_accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String joinEvent = EventMessageBuilder.joinEvent(eventId, editTextPassword.getText().toString());
                networkService.sendMessage(joinEvent);
            }
        });

        //Add the cancel button
        builder.setNegativeButton(context.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.create().show();
    }
}
