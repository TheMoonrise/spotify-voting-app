package com.spotifyplaylistvoting.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.collection.LruCache;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.networking.json.PlaylistMessageBuilder;
import com.spotifyplaylistvoting.networking.model.Track;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.spotifyplaylistvoting.networking.services.NetworkService;

public class PlaylistRecyclerViewAdapter extends RecyclerView.Adapter<PlaylistRecyclerViewAdapter.SongViewHolder> {

    private List<Track> tracks;
    private NetworkService networkService;

    //Request queue for loading the cover images
    private RequestQueue queue;
    private ImageLoader imageLoader;

    private Context context;


    public PlaylistRecyclerViewAdapter(Context context, List<Track> tracks, NetworkService networkService) {

        this.context = context;
        this.tracks = tracks;
        this.networkService = networkService;

        this.queue = Volley.newRequestQueue(context);

        imageLoader = new ImageLoader(queue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(10);

            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });
    }

    public static class SongViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.songCardView) CardView songCardView;

        @BindView(R.id.playlistTextViewSongName) TextView titleTextView;
        @BindView(R.id.playlistTextViewArtist) TextView artistsTextView;
        @BindView(R.id.playlistTextViewExplicit) TextView explicitTextView;
        @BindView(R.id.playlistTextViewInfo) TextView infoTextView;

        @BindView(R.id.playlistTextViewVotes) TextView votesTextView;
        @BindView(R.id.playlistUpVote) ImageView upVote;
        @BindView(R.id.playlistDownVote) ImageView downVote;
        @BindView(R.id.playlitCoverArtFade) ImageView coverArtFade;
        @BindView(R.id.playlistCoverImage) NetworkImageView coverImageView;

        SongViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public SongViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.song_card_view, viewGroup, false);
        return new SongViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final SongViewHolder songViewHolder, int index) {

        final Track track = tracks.get(index);

        //Display the track title
        songViewHolder.titleTextView.setText(track.title);

        //Show and hide the explicit view
        if (track.isExplicit) songViewHolder.explicitTextView.setVisibility(View.VISIBLE);
        else songViewHolder.explicitTextView.setVisibility(View.GONE);

        //Display the subtitle for this song
        StringBuilder artistsBuilder = new StringBuilder();
        for (int i = 0; i < track.artists.length; i++) {
            artistsBuilder.append(track.artists[i]);
            if (i < track.artists.length - 1) artistsBuilder.append(" • ");
        }
        songViewHolder.artistsTextView.setText(artistsBuilder.toString());

        //Display the song duration
        int minutes = (int) (track.duration / 60000);
        int seconds = (int) (track.duration / 1000 % 60);
        String duration = minutes + ":" + (seconds < 10 ? "0" : "") + seconds + "min";
        songViewHolder.infoTextView.setText(duration);

        //Update votes and cover image
        songViewHolder.votesTextView.setText(String.valueOf(track.votes));

        songViewHolder.coverImageView.setDefaultImageResId(R.drawable.default_cover_art);
        songViewHolder.coverImageView.setImageUrl(track.coverImage, imageLoader);

        //Set on click listeners
        songViewHolder.upVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = PlaylistMessageBuilder.voteTrack(track.trackId, true);
                networkService.sendMessage(message);
            }
        });

        songViewHolder.downVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = PlaylistMessageBuilder.voteTrack(track.trackId, false);
                networkService.sendMessage(message);
            }
        });

        //Change layout depending on voting state
        if (track.isVotingOpen) {
            if (track.isVoted) {
                songViewHolder.upVote.setColorFilter(context.getResources().getColor(R.color.tintDisabled));
                songViewHolder.downVote.setColorFilter(context.getResources().getColor(R.color.tintDisabled));

            } else {
                songViewHolder.upVote.setColorFilter(context.getResources().getColor(R.color.tintNone));
                songViewHolder.downVote.setColorFilter(context.getResources().getColor(R.color.tintNone));
            }

            songViewHolder.upVote.setVisibility(View.VISIBLE);
            songViewHolder.downVote.setVisibility(View.VISIBLE);

            songViewHolder.songCardView.setCardBackgroundColor(context.getResources().getColorStateList(R.color.colorBlank));
            songViewHolder.coverArtFade.setImageDrawable(context.getResources().getDrawable(R.drawable.cover_art_fade));

        } else {

            songViewHolder.upVote.setVisibility(View.INVISIBLE);
            songViewHolder.downVote.setVisibility(View.INVISIBLE);

            songViewHolder.songCardView.setCardBackgroundColor(context.getResources().getColorStateList(R.color.colorHighlight));
            songViewHolder.coverArtFade.setImageDrawable(context.getResources().getDrawable(R.drawable.cover_art_fade_highlight));
        }
    }

    @Override
    public int getItemCount() {

        return tracks.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}