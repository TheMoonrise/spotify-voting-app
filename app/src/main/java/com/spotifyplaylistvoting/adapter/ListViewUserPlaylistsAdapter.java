package com.spotifyplaylistvoting.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.volley.toolbox.NetworkImageView;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.spotifyplaylistvoting.R;
import com.spotifyplaylistvoting.helper.VolleySpotifyQueue;
import com.spotifyplaylistvoting.networking.json.PlaylistMessageBuilder;
import com.spotifyplaylistvoting.networking.model.Track;
import com.spotifyplaylistvoting.networking.services.NetworkService;

import java.util.ArrayList;

//Displays the result of search queries to the spotify web api
public class ListViewUserPlaylistsAdapter extends ArrayAdapter<JsonObject> {

    //Layout properties to be bound to
    @BindView(R.id.songCardView) public CardView cardView;
    @BindView(R.id.imageViewSearchCoverArt) public NetworkImageView coverArt;
    @BindView(R.id.playlistTextSearchTitle) public TextView title;
    @BindView(R.id.textSearchSubtitle) public TextView subtitle;

    //Spotify urls
    private static final String PLAYLISTS_URL = "https://api.spotify.com/v1/me/playlists?limit=50";
    private static final String PLAYLISTS_TRACKS_URL = "https://api.spotify.com/v1/playlists/{playlist_id}/tracks";

    //The list holding the json objects to be displayed
    private ArrayList<JsonObject> playlistsJson;

    //The network service to forward results to
    private NetworkService networkService;

    //The context of the attached activity
    private Context context;

    //Request queue for handling server requests
    private VolleySpotifyQueue spotifyQueue;


    public ListViewUserPlaylistsAdapter(Context context, NetworkService networkService, String spotifyAuthKey, ArrayList<JsonObject> playlistsJson) {
        super(context, 0, playlistsJson);

        this.networkService = networkService;
        this.playlistsJson = playlistsJson;
        this.context = context;

        spotifyQueue = new VolleySpotifyQueue(context, spotifyAuthKey);
        spotifyQueue.sendRequest(PLAYLISTS_URL, new VolleySpotifyQueue.ResponseListener() {
            @Override
            public void onResponse(String response) {
                handlePlaylistResponse(response);
            }
        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView != null ? convertView : LayoutInflater.from(context).inflate(R.layout.search_list_entry, parent, false);
        ButterKnife.bind(this, view);

        JsonObject json = playlistsJson.get(position);

        //Read the song title
        final String titleString = json.get("name").getAsString();
        title.setText(titleString);

        //Build the subtitle for the playlist
        int tracks = json.getAsJsonObject("tracks").get("total").getAsInt();
        subtitle.setText(context.getResources().getString(R.string.import_activity_tracks_label, String.valueOf(tracks)));

        //Reade the url for the cover image
        JsonArray images = json.getAsJsonArray("images");
        JsonObject image = images.size() >= 3 ? images.get(2).getAsJsonObject() : images.get(images.size() - 1).getAsJsonObject();

        final String coverImageString = image.get("url").getAsString();
        coverArt.setImageUrl(coverImageString, spotifyQueue.imageLoader());

        //Read additional properties of the playlist
        final String id = json.get("id").getAsString();


        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spotifyQueue.sendRequest(PLAYLISTS_TRACKS_URL.replace("{playlist_id}", id), new VolleySpotifyQueue.ResponseListener() {
                    @Override
                    public void onResponse(String response) {
                        handlePlaylistTracksResponse(response);
                    }
                });
            }
        });

        return view;
    }

    //Cancels all ongoing http requests and unregister any listeners
    public void cleanup() {
        spotifyQueue.cancelRequests();
    }


    //Takes the result of an playlist query and displays them to the view
    private void handlePlaylistResponse(String response) {
        JsonParser parser = new JsonParser();
        JsonArray playlists = parser.parse(response).getAsJsonObject().getAsJsonArray("items");

        playlistsJson.clear();

        for (JsonElement j : playlists) {
            JsonObject playlist = j.getAsJsonObject();
            playlistsJson.add(playlist);
        }

        notifyDataSetChanged();
    }

    //Takes the result of an playlist tracks query and adds them to the event
    private void handlePlaylistTracksResponse(String response) {
        JsonParser parser = new JsonParser();
        JsonArray tracksJson = parser.parse(response).getAsJsonObject().getAsJsonArray("items");

        ArrayList<Track> tracks = new ArrayList<>();
        for (JsonElement j : tracksJson) {
            JsonObject trackJson = j.getAsJsonObject().getAsJsonObject("track");

            Track track = new Track();
            track.title = trackJson.get("name").getAsString();
            track.trackUri = trackJson.get("uri").getAsString();
            track.coverImage = trackJson.getAsJsonObject("album").getAsJsonArray("images").get(2).getAsJsonObject().get("url").getAsString();

            track.duration = trackJson.get("duration_ms").getAsLong();
            track.isExplicit = trackJson.get("explicit").getAsBoolean();

            ArrayList<String> artists = new ArrayList<>();
            for (JsonElement a : trackJson.getAsJsonArray("artists")){
                artists.add(a.getAsJsonObject().get("name").getAsString());
            }

            track.artists = artists.toArray(new String[0]);
            tracks.add(track);
        }

        networkService.sendMessage(PlaylistMessageBuilder.addTracks(tracks.toArray(new Track[0])));
    }
}
